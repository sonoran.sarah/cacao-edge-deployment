package main

import (
	"errors"
	"fmt"
	"os"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/transport"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/spf13/cobra"
)

var (
	rootCmd = &cobra.Command{
		Use:   "go-git-cli [gitURL] [directory]",
		Short: "simple cli to clone git repo using go-git",
		Args:  cobra.ExactArgs(2),
		RunE:  cloneRepo,
	}
	cliParam = struct {
		Branch       string
		Tag          string
		SingleBranch bool
		Depth        int
	}{}
)

func init() {
	rootCmd.Flags().StringVar(&cliParam.Branch, "branch", "master", "branch to clone")
	rootCmd.Flags().StringVar(&cliParam.Tag, "tag", "", "tag to clone")
	rootCmd.Flags().BoolVar(&cliParam.SingleBranch, "single-branch", false, "only clone a single branch")
	rootCmd.Flags().IntVar(&cliParam.Depth, "depth", 1, "depth of history to clone")
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
func cloneRepo(cmd *cobra.Command, args []string) error {
	gitURL := args[0]
	dir := args[1]

	err := mkdirIfNotExist(dir)
	if err != nil {
		return err
	}

	gitAuth, err := getGitAuth()
	if err != nil {
		return err
	}

	var cloneOpts = &git.CloneOptions{
		URL:          gitURL,
		Auth:         gitAuth,
		SingleBranch: cliParam.SingleBranch,
		Depth:        cliParam.Depth,
	}
	if cliParam.Tag != "" {
		cloneOpts.ReferenceName = plumbing.NewTagReferenceName(cliParam.Tag)
	} else {
		cloneOpts.ReferenceName = plumbing.NewBranchReferenceName(cliParam.Branch)
	}

	_, err = git.PlainClone(dir, false, cloneOpts)
	if err != nil {
		if err := os.RemoveAll(dir); err != nil {
			return err
		}
		return err
	}
	return nil
}

func mkdirIfNotExist(dir string) error {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			return err
		}
	}
	return nil
}

func getGitAuth() (transport.AuthMethod, error) {
	username, ok := os.LookupEnv("GIT_USERNAME")
	if ok {
		password, ok := os.LookupEnv("GIT_PASSWORD")
		if !ok {
			return nil, errors.New("missing GIT_PASSWORD")
		}
		return &http.BasicAuth{
			Username: username,
			Password: password,
		}, nil
	}
	return nil, nil
}
