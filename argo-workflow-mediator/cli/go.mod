module gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/cli

go 1.16

require (
	github.com/cloudevents/sdk-go v1.2.0
	github.com/mitchellh/mapstructure v1.4.1
	github.com/nats-io/stan.go v0.9.0
	github.com/rs/xid v1.2.1
	github.com/spf13/cobra v1.1.3
	gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator v0.0.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

replace gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator => ../
