package domain

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// WorkflowResubmitHandler is a msg handler for WorkflowResubmit
type WorkflowResubmitHandler struct {
	argoClis           map[types.Provider]ports.ArgoClient
	eventSink          ports.EventSink
	providerDispatcher ProviderDispatcher
}

// NewWorkflowResubmitHandler creates new WorkflowResubmitHandler
func NewWorkflowResubmitHandler(
	argoClis map[types.Provider]ports.ArgoClient,
	eventSink ports.EventSink,
	providerDispatcher ProviderDispatcher,
) WorkflowResubmitHandler {
	return WorkflowResubmitHandler{argoClis, eventSink, providerDispatcher}
}

// Handle is the handler function
func (h WorkflowResubmitHandler) Handle(event types.WorkflowResubmit) error {
	var err error
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "WorkflowResubmitHandler.Handle",
	})
	// overwrite the provider in the event
	event.Provider = h.providerDispatcher.Dispatch(event.Provider)

	err = h.validateEvent(event)
	if err != nil {
		return err
	}

	newWfName, err := h.resubmitWorkflow(event.Provider, event.WorkflowName)
	if err != nil {
		logger.WithFields(log.Fields{"error": err, "event": event}).Error("fail to resubmit workflow")
		h.publishResubmitFailedEvent(event, types.FailToResubmit, err.Error())
		return err
	}
	logger.WithField("new_workflow", newWfName).Info("workflow resubmitted")

	h.publishResubmittedEvent(event, newWfName)

	return nil
}

func (h WorkflowResubmitHandler) validateEvent(event types.WorkflowResubmit) error {
	err := h.checkEventID(event)
	if err != nil {
		h.publishResubmitFailedEvent(event, types.MissingEventID, err.Error())
		return err
	}

	err = h.checkProvider(event)
	if err != nil {
		h.publishResubmitFailedEvent(event, types.ProviderNotSupported, err.Error())
		return err
	}
	return nil
}

func (h WorkflowResubmitHandler) checkEventID(event types.WorkflowResubmit) error {
	if event.EventID == "" {
		return MissingEventID{event.EventType()}
	}
	return nil
}

func (h WorkflowResubmitHandler) checkProvider(event types.WorkflowResubmit) error {
	var ok bool

	// check if provider is supported
	if _, ok = h.argoClis[event.Provider]; !ok {
		err := UnsupportedProvider{Provider: event.Provider, reason: "provider missing from ArgoClient/ArgoConf"}
		return err
	}

	return nil
}

func (h WorkflowResubmitHandler) resubmitWorkflow(provider types.Provider, wfName string) (string, error) {
	newWf, err := h.argoClis[provider].ResubmitWorkflow(wfName)
	if err != nil {
		return "", err
	}

	return newWf.Name, nil
}

func (h WorkflowResubmitHandler) publishResubmittedEvent(
	reqEvent types.WorkflowResubmit, newWfName string) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "WorkflowResubmitHandler.publishResubmittedEvent",
	})

	event := types.WorkflowResubmitted{
		RequestID:       reqEvent.EventID,
		TransactionID:   reqEvent.TransactionID,
		Provider:        reqEvent.Provider,
		WorkflowName:    reqEvent.WorkflowName,
		NewWorkflowName: newWfName,
	}
	logger = logger.WithField("event_type", event.EventType())

	err := h.eventSink.Publish(event)
	if err != nil {
		logger.WithField("event", event).Error(err)
	} else {
		logger.Info("published event")
	}
}

func (h WorkflowResubmitHandler) publishResubmitFailedEvent(
	reqEvent types.WorkflowResubmit, errType types.EventErrorType, errMsg string) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "WorkflowResubmitHandler.publishResubmitFailedEvent",
	})

	event := types.WorkflowResubmitFailed{
		RequestID:     reqEvent.EventID,
		TransactionID: reqEvent.TransactionID,
		Provider:      reqEvent.Provider,
		WorkflowName:  reqEvent.WorkflowName,
		Error:         errType,
		Msg:           errMsg,
	}
	logger = logger.WithField("event_type", event.EventType())

	err := h.eventSink.Publish(event)
	if err != nil {
		logger.WithField("event", event).Error(err)
	} else {
		logger.Info("published event")
	}
}
