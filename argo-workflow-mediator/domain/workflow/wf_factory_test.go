package workflow

import (
	"gitlab.com/cyverse/cacao-common/common"
	"reflect"
	"testing"

	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/adapters/mock"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// Test creating WorkflowFactory
func TestNewFactory(t *testing.T) {
	var tfBackend ports.TFBackend
	var credStore ports.K8SCredSecretStore
	argoCli := mock.NewMockArgoClient()
	provider := types.Provider("provider-" + xid.New().String())

	type args struct {
		transaction common.TransactionID
		provider    types.Provider
		argoCli     ports.ArgoClient
		wfSrc       ports.WorkflowDefSrc
		wfFilename  string
		tfBackend   ports.TFBackend
		credStore   ports.K8SCredSecretStore
		natsOut     config.NatsConfig
	}
	tests := []struct {
		name     string
		args     args
		wantType reflect.Type
		wantErr  bool
	}{
		{"terraform", args{
			"", provider, argoCli, nil, "terraform",
			tfBackend, credStore, config.NatsConfig{},
		}, reflect.TypeOf(&TerraformFactory{}), false},
		{"terraform.yml", args{
			"", provider, argoCli, nil, "terraform",
			tfBackend, credStore, config.NatsConfig{},
		}, reflect.TypeOf(&TerraformFactory{}), false},
		{"mock", args{
			"", provider, argoCli, nil, "mock",
			tfBackend, credStore, config.NatsConfig{},
		}, reflect.TypeOf(&MockFactory{}), false},
		{"mock.yml", args{
			"", provider, argoCli, nil, "mock.yml",
			tfBackend, credStore, config.NatsConfig{},
		}, reflect.TypeOf(&MockFactory{}), false},
		{"empty", args{
			"", provider, argoCli, nil, "",
			tfBackend, credStore, config.NatsConfig{},
		}, nil, true},
		{"unknown", args{
			"", provider, argoCli, nil, "",
			tfBackend, credStore, config.NatsConfig{},
		}, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewFactory(tt.args.transaction, tt.args.provider, tt.args.argoCli, tt.args.wfSrc, tt.args.wfFilename, tt.args.tfBackend, tt.args.credStore, tt.args.natsOut)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewFactory() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && reflect.TypeOf(got) != tt.wantType {
				t.Errorf("NewFactory() = %v, want %v", reflect.TypeOf(got), tt.wantType)
			}
		})
	}
}
