package workflow

import (
	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
)

// InjectWorkflowParameter injects input parameter into workflow definition
func InjectWorkflowParameter(wfDef *wfv1.Workflow, name string, value string) *wfv1.Workflow {
	var param *wfv1.Parameter
	param = findWorkflowParameter(wfDef, name)

	// create if not exist
	if param == nil {
		param = &wfv1.Parameter{}
		param.Name = name
		param.Value = new(wfv1.AnyString)
		*(param.Value) = wfv1.AnyString(value)
		wfDef.Spec.Arguments.Parameters = append(wfDef.Spec.Arguments.Parameters, *param)
	} else {
		if param.Value == nil {
			param.Value = new(wfv1.AnyString)
		}
		*(param.Value) = wfv1.AnyString(value)
	}

	return wfDef
}

func findWorkflowParameter(wfDef *wfv1.Workflow, name string) *wfv1.Parameter {
	for _, param := range wfDef.Spec.Arguments.Parameters {
		if param.Name == name {
			return &param
		}
	}
	return nil
}
