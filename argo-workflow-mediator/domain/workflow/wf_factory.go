package workflow

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// Factory is factory for creating Argo Workflows
type Factory interface {
	Provider() types.Provider

	// Creates a workflow with given data, returns the workflow name, and possible error
	Create(data map[string]interface{}) (string, error)
}

// NewFactory creates a Factory from a WorkflowDefSrc
func NewFactory(
	transaction common.TransactionID,
	provider types.Provider,
	argoCli ports.ArgoClient,
	wfSrc ports.WorkflowDefSrc,
	wfFilename string,
	tfBackend ports.TFBackend,
	credStore ports.K8SCredSecretStore,
	natsOut config.NatsConfig) (Factory, error) {

	var fac Factory
	var err error

	switch wfFilename {
	case "imaging.yml":
		panic("FIXME")
	case "ansible_deploy.yml":
		panic("FIXME")
	case "terraform":
		fallthrough
	case "terraform.yml":
		fac, err = NewTerraformFactory(transaction, provider, argoCli, wfSrc, tfBackend, credStore, natsOut)
		if err != nil {
			return nil, err
		}
		return fac, nil
	case "mock":
		fallthrough
	case "mock.yml":
		fac, err = NewMockFactory(provider, argoCli, wfSrc)
		if err != nil {
			return nil, err
		}
		return fac, nil
	default:
		return nil, UnknownWorkflow{wfName: wfFilename}
	}
}

// Data is the data required to create a workflow of specific type
type Data = map[string]interface{}
