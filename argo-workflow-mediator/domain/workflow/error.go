package workflow

import (
	"fmt"
)

// UnknownWorkflow indicates the type of workflow is unknown and unsupported
type UnknownWorkflow struct {
	wfName string
}

func (err UnknownWorkflow) Error() string {
	return fmt.Sprintf("Unknown Workflow: %s", err.wfName)
}

// FailToLoadWorkflowDef indicates mediator is unable to load workflow def
type FailToLoadWorkflowDef struct {
	WfName string
}

func (err FailToLoadWorkflowDef) Error() string {
	return fmt.Sprintf("Fail to load workflow definition: %s", err.WfName)
}

// DataError indicates there is an error in the workflow data
type DataError struct {
	Field string
}

func (err DataError) Error() string {
	return fmt.Sprintf("Error in workflow data, field %s", err.Field)
}
