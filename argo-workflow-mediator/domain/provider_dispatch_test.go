package domain

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
	"testing"
)

func TestProviderDispatcher_Dispatch(t *testing.T) {
	type fields struct {
		defaultProvider types.Provider
		providerConf    map[types.Provider]config.ProviderConfig
	}
	type args struct {
		eventProvider types.Provider
	}
	provider1 := types.Provider(common.NewID("provider"))
	provider2 := types.Provider(common.NewID("provider"))
	tests := []struct {
		name   string
		fields fields
		args   args
		want   types.Provider
	}{
		{
			name: "in-config-default",
			fields: fields{
				defaultProvider: provider1,
				providerConf: map[types.Provider]config.ProviderConfig{
					provider1: {},
					provider2: {},
				},
			},
			args: args{
				eventProvider: provider1,
			},
			want: provider1,
		},
		{
			name: "in-config",
			fields: fields{
				defaultProvider: provider2,
				providerConf: map[types.Provider]config.ProviderConfig{
					provider1: {},
					provider2: {},
				},
			},
			args: args{
				eventProvider: provider1,
			},
			want: provider1,
		},
		{
			name: "not-in-config",
			fields: fields{
				defaultProvider: provider1,
				providerConf: map[types.Provider]config.ProviderConfig{
					provider1: {},
				},
			},
			args: args{
				eventProvider: provider2,
			},
			want: provider1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := ProviderDispatcher{
				defaultProvider: tt.fields.defaultProvider,
				providerConf:    tt.fields.providerConf,
			}
			if got := d.Dispatch(tt.args.eventProvider); got != tt.want {
				t.Errorf("Dispatch() = %v, want %v", got, tt.want)
			}
		})
	}
}
