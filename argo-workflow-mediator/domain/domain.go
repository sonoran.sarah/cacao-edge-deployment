package domain

import (
	"fmt"
	"reflect"
	"sync"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// Domain is the entrypoint to the domain logic/service
type Domain struct {
	// Number of event worker to spawn
	EventWorkerCount uint
	// Buffer capacity of the go channel for events
	EventChannelBufferCapacity uint
	// Event adapter, outgoing
	EventSink ports.EventSink
	// Event adapter, incoming
	EventSrc ports.EventSrc
	// Argo clients for all providers
	ArgoClis map[types.Provider]ports.ArgoClient
	// Source for workflow definition for all providers
	WfSrcs map[types.Provider]ports.WorkflowDefSrc
	// Terraform backend
	TfBackend ports.TFBackend
	// K8S Credential Secret Store for all providers
	CredStores map[types.Provider]ports.K8SCredSecretStore
	// channel for incoming events
	eventSrcChan chan types.Event
	// config for outbound NATS
	natsOut            config.NatsConfig
	providerDispatcher ProviderDispatcher
}

// NewDomain creates new Domain object
func NewDomain() Domain {
	domain := Domain{
		ArgoClis:   make(map[types.Provider]ports.ArgoClient),
		WfSrcs:     make(map[types.Provider]ports.WorkflowDefSrc),
		CredStores: make(map[types.Provider]ports.K8SCredSecretStore),
	}
	return domain
}

// Init initialize domain.
func (domain *Domain) Init(conf config.Config) error {

	if domain.EventWorkerCount == 0 {
		return fmt.Errorf("EventWorkerCount is 0")
	}
	domain.providerDispatcher = NewProviderDispatcher(conf.ProviderConf, conf.DefaultProvider)

	// Init argo clients for all provider
	for provider := range conf.ProviderConf {
		domain.ArgoClis[provider].Init(config.ArgoConfigFromProviderConfig(conf.ProviderConf[provider]))
	}

	// Init workflow definition source for all provider
	for provider := range conf.ProviderConf {
		err := domain.WfSrcs[provider].Init(config.WorkflowConfig{WorkflowBaseDir: conf.WorkflowBaseDir})
		if err != nil {
			return err
		}
	}

	// Init k8s secret adapters for all provider
	for provider := range conf.ProviderConf {
		k8sConf, err := config.K8SConfigFromProviderConfig(conf.ProviderConf[provider])
		if err != nil {
			return err
		}
		err = domain.CredStores[provider].Init(*k8sConf)
		if err != nil {
			return err
		}
	}

	// Init terraform backend
	err := domain.TfBackend.Init(conf.TFBackend)
	if err != nil {
		return err
	}

	// Init event sink
	domain.EventSink.InitSink(conf.NatsOutConf)

	// Init event source
	domain.EventSrc.InitSrc(conf.NatsInConf)

	domain.eventSrcChan = make(chan types.Event, domain.EventChannelBufferCapacity)
	domain.EventSrc.InitChannel(domain.eventSrcChan)

	// store the outbound NATS config
	domain.natsOut = conf.NatsOutConf

	return nil
}

// Start the domain service
func (domain Domain) Start() {

	var wg sync.WaitGroup

	err := domain.EventSrc.Start()
	if err != nil {
		log.Fatal(err)
	}

	domain.spawnEventWorker(&wg)

	wg.Wait()
}

func (domain Domain) spawnEventWorker(wg *sync.WaitGroup) {
	for i := uint(0); i < domain.EventWorkerCount; i++ {
		wg.Add(1)
		go domain.processEvent(wg)
	}
}

func (domain Domain) processEvent(wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "domain.processEvent",
	})
	logger.Info("starting domain.processEvent()")
	defer logger.Info("domain.processEvent() exiting")
	defer wg.Done()

	for event := range domain.eventSrcChan {
		logger.WithFields(log.Fields{"eventType": event.EventType()}).Info("received event")

		domain.dispatchEvent(event)
	}
	return nil
}

// dispatchEvent dispatch events to corrsponding handler
func (domain Domain) dispatchEvent(event types.Event) {
	logger := log.WithField("event_type", event.EventType())
	switch request := event.(type) {
	case types.WorkflowCreate:
		handler := NewWorkflowCreateHandler(domain)
		err := handler.Handle(request)
		if err != nil {
			logger.WithField("error_type", reflect.TypeOf(err)).Error(err)
		}
	case types.WorkflowTerminate:
		handler := NewWorkflowTerminateHandler(domain.ArgoClis, domain.EventSink, domain.providerDispatcher)
		err := handler.Handle(request)
		if err != nil {
			logger.WithField("error_type", reflect.TypeOf(err)).Error(err)
		}
	case types.WorkflowResubmit:
		handler := NewWorkflowResubmitHandler(domain.ArgoClis, domain.EventSink, domain.providerDispatcher)
		err := handler.Handle(request)
		if err != nil {
			logger.WithField("error_type", reflect.TypeOf(err)).Error(err)
		}
	default:
		logger.Trace("unspported event type")
	}
}
