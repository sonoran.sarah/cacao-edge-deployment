package domain

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// WorkflowTerminateHandler is handler for WorkflowTerminateCmd
type WorkflowTerminateHandler struct {
	argoClis map[types.Provider]ports.ArgoClient
	// sink to publish the workflow terminated event into
	eventSink          ports.EventSink
	providerDispatcher ProviderDispatcher
}

// NewWorkflowTerminateHandler creates a new WorkflowTerminateCmdHandler
func NewWorkflowTerminateHandler(
	argoClis map[types.Provider]ports.ArgoClient,
	eventSink ports.EventSink,
	providerDispatcher ProviderDispatcher,
) WorkflowTerminateHandler {
	return WorkflowTerminateHandler{argoClis, eventSink, providerDispatcher}
}

// Handle is the handler function, same signature as ports.MsgHandler
func (h WorkflowTerminateHandler) Handle(event types.WorkflowTerminate) error {
	var err error
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "WorkflowTerminateHandler.Handle",
	})
	// overwrite the provider in the event
	event.Provider = h.providerDispatcher.Dispatch(event.Provider)

	err = h.validateEvent(event)
	if err != nil {
		return err
	}

	err = h.terminateWorkflow(event.Provider, event.WorkflowName)
	if err != nil {
		h.publishTerminateFailedEvent(event, types.FailToTerminate, err.Error())
		return err
	}

	status, err := h.workflowStatus(event.Provider, event.WorkflowName)
	if err != nil {
		logger.WithFields(log.Fields{"workflow": event.WorkflowName, "status_check_err": err}).Info("workflow terminated")
		h.publishTerminateFailedEvent(event, types.FailToRetrieveStatus, err.Error())
		return err
	}
	logger.WithFields(log.Fields{"workflow": event.WorkflowName, "status": status}).Info("workflow terminated")

	err = h.publishTerminatedEvent(event, status)
	if err != nil {
		return err
	}
	return nil
}

func (h WorkflowTerminateHandler) validateEvent(event types.WorkflowTerminate) error {
	err := h.checkEventID(event)
	if err != nil {
		h.publishTerminateFailedEvent(event, types.MissingEventID, err.Error())
		return err
	}

	err = h.checkProvider(event)
	if err != nil {
		h.publishTerminateFailedEvent(event, types.ProviderNotSupported, err.Error())
		return err
	}
	return nil
}

func (h WorkflowTerminateHandler) checkEventID(event types.WorkflowTerminate) error {
	if event.EventID == "" {
		return MissingEventID{event.EventType()}
	}
	return nil
}

func (h WorkflowTerminateHandler) checkProvider(event types.WorkflowTerminate) error {
	var ok bool

	// check if provider is supported
	if _, ok = h.argoClis[event.Provider]; !ok {
		err := UnsupportedProvider{Provider: event.Provider, reason: "provider missing from ArgoClient/ArgoConf"}
		return err
	}

	return nil
}

func (h WorkflowTerminateHandler) terminateWorkflow(provider types.Provider, wfName string) error {
	err := h.argoClis[provider].TerminateWorkflow(wfName)
	if err != nil {
		return err
	}
	return nil
}

func (h WorkflowTerminateHandler) workflowStatus(provider types.Provider, wfName string) (string, error) {
	status, err := h.argoClis[provider].WorkflowStatus(wfName)
	if err != nil {
		return "", err
	}
	return string(status.Phase), nil
}

// Publish WorkflowTerminated event
func (h WorkflowTerminateHandler) publishTerminatedEvent(
	reqEvent types.WorkflowTerminate, status string) error {
	event := types.WorkflowTerminated{
		RequestID:      reqEvent.EventID,
		TransactionID:  reqEvent.TransactionID,
		Provider:       reqEvent.Provider,
		WorkflowName:   reqEvent.WorkflowName,
		WorkflowStatus: status,
	}
	return h.eventSink.Publish(event)
}

// Publish WorkflowTerminateFailed event
func (h WorkflowTerminateHandler) publishTerminateFailedEvent(
	reqEvent types.WorkflowTerminate, errType types.EventErrorType, errMsg string) error {
	event := types.WorkflowTerminateFailed{
		RequestID:     reqEvent.EventID,
		TransactionID: reqEvent.TransactionID,
		Provider:      reqEvent.Provider,
		WorkflowName:  reqEvent.WorkflowName,
		Error:         errType,
		Msg:           errMsg,
	}
	return h.eventSink.Publish(event)
}
