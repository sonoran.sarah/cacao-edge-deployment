package domain

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"reflect"
	"testing"

	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/adapters/mock"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/domain/workflow"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

type terminateFields struct {
	argoClis           map[types.Provider]ports.ArgoClient
	argoCli            mock.ArgoClient
	eventSink          mock.EventSink
	wfName             string
	provider           types.Provider
	providerDispatcher ProviderDispatcher
}
type terminateArgs struct {
	event types.WorkflowTerminate
}

func setupTerminateFields(wfName string, provider types.Provider) terminateFields {
	argoCli := mock.NewMockArgoClient()
	argoClis := make(map[types.Provider]ports.ArgoClient)
	argoClis[provider] = argoCli
	eventSink := mock.NewMockEventSink()

	// create workflow
	wfDef := workflow.HelloWorldWorkflow.DeepCopy()
	wfDef.Name = "mock-wf-"
	wf, err := argoCli.CreateWorkflowWithName(wfName, wfDef)
	if err != nil {
		panic(err)
	}
	// modify workflow name
	result, _ := argoCli.GetWorkflow(wf.Name)
	result.Name = wfName

	wf.Name = wfName

	providerDispatcher := NewProviderDispatcher(
		map[types.Provider]config.ProviderConfig{
			provider: {},
		}, provider)
	return terminateFields{argoClis, argoCli, eventSink, wfName, provider, providerDispatcher}
}

func TestWorkflowTerminateHandler(t *testing.T) {

	wfName := "mock-wf-123"
	provider := types.Provider("provider-" + xid.New().String())

	tests := []struct {
		name         string
		fields       terminateFields
		args         terminateArgs
		postRunCheck func(*testing.T, terminateFields, terminateArgs)
		wantErr      bool
		errType      reflect.Type
	}{
		{"terminate", setupTerminateFields(wfName, provider),
			terminateArgs{types.WorkflowTerminate{EventID: "test-event-id-123", Provider: provider, WorkflowName: wfName}},
			terminateCheck, false, nil},
		{"non-exist", setupTerminateFields(wfName, provider),
			terminateArgs{types.WorkflowTerminate{EventID: "test-event-id-123", Provider: provider, WorkflowName: "non-exist"}},
			terminateNonexistCheck, true, reflect.TypeOf(mock.WorkflowNotFound{})},
		{"missing event id", setupTerminateFields(wfName, provider),
			terminateArgs{types.WorkflowTerminate{EventID: "", Provider: provider, WorkflowName: wfName}},
			terminateMissingEventIDCheck, true, reflect.TypeOf(MissingEventID{})},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := WorkflowTerminateHandler{
				argoClis:           tt.fields.argoClis,
				eventSink:          tt.fields.eventSink,
				providerDispatcher: tt.fields.providerDispatcher,
			}
			err := h.Handle(tt.args.event)
			if (err != nil) != tt.wantErr {
				t.Errorf("WorkflowTerminateHandler.Handle() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr && tt.errType != reflect.TypeOf(err) {
				t.Fatalf("unexpected error = %v, want %v", reflect.TypeOf(err), tt.errType)
			}
			if tt.postRunCheck != nil {
				tt.postRunCheck(t, tt.fields, tt.args)
			}
		})
	}
}

func terminateCheck(t *testing.T, fields terminateFields, args terminateArgs) {
	eventSink := fields.eventSink
	wfName := fields.wfName
	provider := fields.provider

	// check event sink
	emittedEvent := eventSink.GetEvent()
	assert.NotNil(t, emittedEvent, "no event in the event sink")
	assert.Equal(t, types.WorkflowTerminatedEvent, emittedEvent.EventType(), "inconsistent event type")

	wfTerminatedEvent, ok := emittedEvent.(types.WorkflowTerminated)
	assert.Truef(t, ok, "cannot convert to %s, is of type %s", reflect.TypeOf(types.WorkflowTerminated{}), reflect.TypeOf(emittedEvent))

	// check workflow name in emitted event
	assert.Equal(t, wfName, wfTerminatedEvent.WorkflowName, "inconsistent workflow name")
	// check provider in emitted event
	assert.Equal(t, provider, wfTerminatedEvent.Provider, "inconsistent provider")
}

func terminateNonexistCheck(t *testing.T, fields terminateFields, args terminateArgs) {
	eventSink := fields.eventSink
	wfName := args.event.WorkflowName
	provider := fields.provider

	// check event sink
	emittedEvent := eventSink.GetEvent()
	assert.NotNil(t, emittedEvent, "no event in the event sink")
	assert.Equal(t, types.WorkflowTerminateFailedEvent, emittedEvent.EventType(), "inconsistent event type")

	failureEvent, ok := emittedEvent.(types.WorkflowTerminateFailed)
	assert.Truef(t, ok, "cannot convert to %s, is of type %s", reflect.TypeOf(types.WorkflowTerminateFailed{}), reflect.TypeOf(emittedEvent))

	// check workflow name in emitted event
	assert.Equal(t, wfName, failureEvent.WorkflowName, "inconsistent workflow name")
	// check provider in emitted event
	assert.Equal(t, provider, failureEvent.Provider, "inconsistent provider")
	// check error type in emitted event
	assert.Equal(t, types.EventErrorType("FailToTerminate"), failureEvent.Error, "inconsistent error type")
}

func terminateMissingEventIDCheck(t *testing.T, fields terminateFields, args terminateArgs) {
	eventSink := fields.eventSink
	wfName := fields.wfName
	provider := fields.provider

	// check event sink
	emittedEvent := eventSink.GetEvent()
	assert.NotNil(t, emittedEvent, "no event in the event sink")
	assert.Equal(t, types.WorkflowTerminateFailedEvent, emittedEvent.EventType(), "inconsistent event type")

	failureEvent, ok := emittedEvent.(types.WorkflowTerminateFailed)
	assert.Truef(t, ok, "cannot convert to %s, is of type %s", reflect.TypeOf(types.WorkflowTerminateFailed{}), reflect.TypeOf(emittedEvent))

	// check workflow name in emitted event
	assert.Equal(t, wfName, failureEvent.WorkflowName, "inconsistent workflow name")
	// check provider in emitted event
	assert.Equal(t, provider, failureEvent.Provider, "inconsistent provider")
	// check error type in emitted event
	assert.Equal(t, types.EventErrorType("MissingEventID"), failureEvent.Error, "inconsistent error type")
}
