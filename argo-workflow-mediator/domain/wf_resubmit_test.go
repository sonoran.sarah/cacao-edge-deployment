package domain

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"reflect"
	"testing"

	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/adapters/mock"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/domain/workflow"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

type resubmitFields struct {
	argoClis           map[types.Provider]ports.ArgoClient
	argoCli            mock.ArgoClient
	eventSink          mock.EventSink
	wfName             string
	provider           types.Provider
	providerDispatcher ProviderDispatcher
}

type resubmitArgs struct {
	event types.WorkflowResubmit
}

func setupResubmitFields(wfName string, provider types.Provider) resubmitFields {
	argoCli := mock.NewMockArgoClient()
	argoClis := make(map[types.Provider]ports.ArgoClient)
	argoClis[provider] = argoCli
	eventSink := mock.NewMockEventSink()

	// create workflow
	wfDef := workflow.HelloWorldWorkflow.DeepCopy()
	wfDef.Name = "mock-wf-"
	wf, err := argoCli.CreateWorkflowWithName(wfName, wfDef)
	if err != nil {
		panic(err)
	}
	// modify workflow name
	result, _ := argoCli.GetWorkflow(wf.Name)
	result.Name = wfName

	wf.Name = wfName
	providerDispatcher := NewProviderDispatcher(
		map[types.Provider]config.ProviderConfig{
			provider: {},
		}, provider)
	return resubmitFields{argoClis, argoCli, eventSink, wfName, provider, providerDispatcher}
}

func TestWorkflowResubmitHandler(t *testing.T) {

	wfName := "mock-wf-123"
	provider := types.Provider("provider-" + xid.New().String())

	tests := []struct {
		name         string
		fields       resubmitFields
		args         resubmitArgs
		postRunCheck func(*testing.T, resubmitFields, resubmitArgs)
		wantErr      bool
	}{
		{"resubmit", setupResubmitFields(wfName, provider),
			resubmitArgs{types.WorkflowResubmit{EventID: "test-event-id-123", Provider: provider, WorkflowName: wfName}},
			resubmitCheck, false},
		{"missing event id", setupResubmitFields(wfName, provider),
			resubmitArgs{types.WorkflowResubmit{EventID: "", Provider: provider, WorkflowName: wfName}},
			resubmitFailedCheck, true},
		{"non-exist", setupResubmitFields(wfName, provider),
			resubmitArgs{types.WorkflowResubmit{EventID: "test-event-id-123", Provider: provider, WorkflowName: "non-exist"}},
			resubmitFailedCheck, true},
		{"empty", setupResubmitFields(wfName, provider),
			resubmitArgs{types.WorkflowResubmit{EventID: "test-event-id-123", Provider: provider, WorkflowName: ""}},
			resubmitFailedCheck, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := WorkflowResubmitHandler{
				argoClis:           tt.fields.argoClis,
				eventSink:          tt.fields.eventSink,
				providerDispatcher: tt.fields.providerDispatcher,
			}
			if err := h.Handle(tt.args.event); (err != nil) != tt.wantErr {
				t.Errorf("WorkflowResubmitHandler.Handle() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.postRunCheck != nil {
				tt.postRunCheck(t, tt.fields, tt.args)
			}
		})
	}
}

func resubmitCheck(t *testing.T, fields resubmitFields, args resubmitArgs) {
	eventSink := fields.eventSink
	argoCli := fields.argoCli
	wfName := fields.wfName
	provider := fields.provider

	// check event sink
	emittedEvent := eventSink.GetEvent()
	assert.NotNil(t, emittedEvent, "no event in the event sink")
	assert.Equal(t, types.WorkflowResubmittedEvent, emittedEvent.EventType(), "inconsistent event type")

	wfResubmittedEvent, ok := emittedEvent.(types.WorkflowResubmitted)
	assert.Truef(t, ok, "cannot convert to %s, is of type %s", reflect.TypeOf(types.WorkflowTerminated{}), reflect.TypeOf(emittedEvent))

	// check workflow name in emitted event
	assert.Equal(t, wfName, wfResubmittedEvent.WorkflowName, "inconsistent (old) workflow name")
	// check provider in emitted event
	assert.Equal(t, provider, wfResubmittedEvent.Provider, "inconsistent provider")

	// check new workflow name in emitted event
	assert.NotEqual(t, "", wfResubmittedEvent.NewWorkflowName, "empty new workflow name")

	newWf, err := argoCli.GetWorkflow(wfResubmittedEvent.NewWorkflowName)
	assert.NoError(t, err)
	assert.Equal(t, wfResubmittedEvent.NewWorkflowName, newWf.Name, "inconsistent workflow name")
	// check request id in emitted event
	assert.Equal(t, args.event.EventID, wfResubmittedEvent.RequestID, "inconsistent request id")
}

func resubmitFailedCheck(t *testing.T, fields resubmitFields, args resubmitArgs) {
	eventSink := fields.eventSink
	wfName := args.event.WorkflowName
	provider := fields.provider

	// check event sink
	emittedEvent := eventSink.GetEvent()
	assert.NotNil(t, emittedEvent, "no event in the event sink")
	assert.Equal(t, types.WorkflowResubmitFailedEvent, emittedEvent.EventType(), "inconsistent event type")

	wfResubmitFailedEvent, ok := emittedEvent.(types.WorkflowResubmitFailed)
	assert.Truef(t, ok, "cannot convert to %s, is of type %s", reflect.TypeOf(types.WorkflowTerminated{}), reflect.TypeOf(emittedEvent))

	// check workflow name in emitted event
	assert.Equal(t, wfName, wfResubmitFailedEvent.WorkflowName, "inconsistent (old) workflow name")
	// check provider in emitted event
	assert.Equal(t, provider, wfResubmitFailedEvent.Provider, "inconsistent provider")

	// check request id in emitted event
	assert.Equal(t, args.event.EventID, wfResubmitFailedEvent.RequestID, "inconsistent request id")
}
