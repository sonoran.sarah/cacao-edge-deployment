package domain

import (
	"encoding/base64"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"

	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/adapters/mock"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/domain/workflow"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/utils"
)

func setupDomain(provider types.Provider, argoCli ports.ArgoClient,
	wfSrc ports.WorkflowDefSrc, eventSink ports.EventSink,
	tfBackend ports.TFBackend, credStore ports.K8SCredSecretStore) Domain {

	domain := Domain{
		EventSink: eventSink,
		ArgoClis: map[types.Provider]ports.ArgoClient{
			provider: argoCli,
		},
		WfSrcs: map[types.Provider]ports.WorkflowDefSrc{
			provider: wfSrc,
		},
		TfBackend: tfBackend,
		CredStores: map[types.Provider]ports.K8SCredSecretStore{
			provider: credStore,
		},
		providerDispatcher: NewProviderDispatcher(
			map[types.Provider]config.ProviderConfig{
				provider: {},
			}, provider),
	}
	return domain
}

func openstackCredBase64(key []byte) string {
	cred := types.OpenStackCredential{
		IdentityAPIVersion: "3",
		RegionName:         "region",
		AuthURL:            "https://openstack.org",
		ProjectDomainID:    "project-domain",
		ProjectID:          "project-id-123",
		ProjectName:        "project-name",
		UserDomainName:     "user-domain",
		Username:           "username",
		Password:           "password",
	}
	credJSONStr, err := json.Marshal(cred)
	if err != nil {
		panic(err)
	}

	credCiphertext, err := utils.AESEncrypt(key, credJSONStr)
	if err != nil {
		panic(err)
	}
	return base64.StdEncoding.EncodeToString(credCiphertext)
}

type createFields struct {
	domain      Domain
	natsOut     config.NatsConfig
	provider    types.Provider
	mockArgoCli mock.ArgoClient
	wfSrc       *mock.WorkflowDefSrc
	eventSink   mock.EventSink
	credStore   mock.K8SCredSecretStore
}
type createArgs struct {
	event types.WorkflowCreate
}

func setupCreateFields(provider types.Provider, key []byte, wfFilename string) createFields {
	mockArgoCli := mock.NewMockArgoClient()
	wfSrc := mock.NewMockWorkflowDefSrc()
	wfSrc.Register(wfFilename, &workflow.HelloWorldWorkflow)
	eventSink := mock.NewMockEventSink()
	credStore := mock.K8SCredSecretStore{}
	credStore.Init(config.K8SConfig{})
	credStore.SetKey(key)
	tfBackend := &mock.TFMockBackend{}
	err := tfBackend.Init(config.TFBackendConfig{})
	if err != nil {
		panic(err)
	}

	domain := setupDomain(provider, mockArgoCli, wfSrc, eventSink, tfBackend, &credStore)
	return createFields{domain, config.NatsConfig{}, provider, mockArgoCli, wfSrc, eventSink, credStore}
}

func createEvents(provider types.Provider, encryptKey []byte) map[string]types.WorkflowCreate {

	payload := make(map[string]interface{})
	payload["module_name"] = "openstack-module"
	payload["username"] = "test_user1"
	payload["credential_secrets"] = "secret1"
	ansibleVars := make(map[string]string)
	ansibleVars["key"] = "value"
	payload["ansible_vars"] = ansibleVars
	createMock := types.WorkflowCreate{EventID: "test-event-id-123", Provider: provider, Username: nil, WorkflowFilename: "mock.yml", WfDat: payload}

	username := "test_user1"
	payload = make(map[string]interface{})
	payload["deployment_id"] = "d12345"
	payload["template_id"] = "t12345"
	payload["tf_state_key"] = "tf12345"
	payload["git_url"] = "https://gitlab.com/cyverse/foobar"
	payload["git_upstream"] = map[string]string{
		"branch": "master",
	}
	payload["sub_path"] = "openstack-module"
	payload["username"] = username
	payload["cloud_cred_id"] = "secret1"
	ansibleVars = make(map[string]string)
	ansibleVars["key"] = "value"
	payload["ansible_vars"] = ansibleVars
	payload["cloud_cred"] = openstackCredBase64(encryptKey)
	createTerraform := types.WorkflowCreate{EventID: "test-event-id-123", Provider: provider, Username: &username, WorkflowFilename: "terraform.yml", WfDat: payload}

	username = "test_user1"
	payload = make(map[string]interface{})
	payload["template_id"] = "t12345"
	payload["module_name"] = "openstack-module"
	payload["username"] = "test_user1"
	payload["credential_secrets"] = "secret1"
	ansibleVars = make(map[string]string)
	ansibleVars["key"] = "value"
	payload["ansible_vars"] = ansibleVars
	createNonexist := types.WorkflowCreate{EventID: "test-event-id-123", Provider: provider, Username: &username, WorkflowFilename: "FooBar.yml", WfDat: payload}

	username = "test_user1"
	payload = make(map[string]interface{})
	payload["template_id"] = "t12345"
	payload["module_name"] = "openstack-module"
	payload["username"] = "test_user1"
	payload["credential_secrets"] = "secret1"
	ansibleVars = make(map[string]string)
	ansibleVars["key"] = "value"
	payload["ansible_vars"] = ansibleVars
	createMissingEventID := types.WorkflowCreate{Provider: provider, Username: &username, WorkflowFilename: "mock.yml", WfDat: payload}

	return map[string]types.WorkflowCreate{
		"mock":             createMock,
		"terraform":        createTerraform,
		"non-exist":        createNonexist,
		"missing event id": createMissingEventID,
	}
}

func TestWorkflowCreateHandler_Handle(t *testing.T) {

	provider := types.Provider("provider-" + xid.New().String())
	key := utils.GenerateAES256Key()

	tests := []struct {
		name         string
		fields       createFields
		args         createArgs
		postRunCheck func(t *testing.T, fields createFields, args createArgs)
		wantErr      bool
		errType      reflect.Type
	}{
		{"mock", setupCreateFields(provider, key, "mock.yml"),
			createArgs{createEvents(provider, key)["mock"]},
			createMockCheck, false, nil},
		{"terraform", setupCreateFields(provider, key, "terraform.yml"),
			createArgs{createEvents(provider, key)["terraform"]},
			createTerraformCheck, false, nil},
		{"provider-not-in-config", setupCreateFields(provider, key, "terraform.yml"),
			createArgs{createEvents(types.Provider("provider-"+xid.New().String()), key)["terraform"]},
			createTerraformCheck, false, nil},
		{"non-exist", setupCreateFields(provider, key, "terraform.yml"),
			createArgs{createEvents(provider, key)["non-exist"]},
			createNonExistCheck, true, reflect.TypeOf(workflow.UnknownWorkflow{})},
		{"missing event id", setupCreateFields(provider, key, "terraform.yml"),
			createArgs{createEvents(provider, key)["missing event id"]},
			createMissingEventIDCheck, true, reflect.TypeOf(MissingEventID{})},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := NewWorkflowCreateHandler(tt.fields.domain)
			err := h.Handle(tt.args.event)
			if (err != nil) != tt.wantErr {
				t.Errorf("WorkflowCreateHandler.Handle() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr && reflect.TypeOf(err) != tt.errType {
				t.Errorf("unexpected error = %v, want %v", reflect.TypeOf(err), tt.errType)
			}
			if tt.postRunCheck != nil {
				tt.postRunCheck(t, tt.fields, tt.args)
			}
		})
	}
}

func createMockCheck(t *testing.T, fields createFields, args createArgs) {
	provider := fields.provider
	mockArgoCli := fields.mockArgoCli
	eventSink := fields.eventSink

	// check argo workflow
	wfNames := mockArgoCli.ListWorkflowNames()
	assert.Lenf(t, wfNames, 1, "there should only be 1 workflow created, %d", len(wfNames))

	// check event sink
	emittedEvent := eventSink.GetEvent()
	assert.NotNil(t, emittedEvent, "no event in the event sink")
	assert.Equalf(t, types.WorkflowCreatedEvent, emittedEvent.EventType(), "inconsistent event type, %s", emittedEvent.EventType())

	wfCreatedEvent, ok := emittedEvent.(types.WorkflowCreated)
	assert.Truef(t, ok, "cannot convert to %s, is of type %s", reflect.TypeOf(types.WorkflowCreated{}), reflect.TypeOf(emittedEvent))
	assert.Equal(t, wfNames[0], wfCreatedEvent.WorkflowName, "inconsistent workflow name")
	assert.Equal(t, provider, wfCreatedEvent.Provider, "inconsistent provider")
}

func createTerraformCheck(t *testing.T, fields createFields, args createArgs) {
	provider := fields.provider
	mockArgoCli := fields.mockArgoCli
	eventSink := fields.eventSink

	// check argo workflow
	wfNames := mockArgoCli.ListWorkflowNames()
	assert.Lenf(t, wfNames, 1, "there should only be 1 workflow created")

	// check event sink
	emittedEvent := eventSink.GetEvent()
	assert.NotNil(t, emittedEvent, "no event in the event sink")
	assert.Equal(t, types.WorkflowCreatedEvent, emittedEvent.EventType(), "inconsistent event type")

	wfCreatedEvent, ok := emittedEvent.(types.WorkflowCreated)
	assert.Truef(t, ok, "cannot convert to %s, is of type %s", reflect.TypeOf(types.WorkflowCreated{}), reflect.TypeOf(emittedEvent))
	assert.Equal(t, wfNames[0], wfCreatedEvent.WorkflowName, "inconsistent workflow name")
	assert.Equal(t, provider, wfCreatedEvent.Provider, "inconsistent provider")
}

func createNonExistCheck(t *testing.T, fields createFields, args createArgs) {
	provider := fields.provider
	mockArgoCli := fields.mockArgoCli
	eventSink := fields.eventSink

	// check argo workflow
	wfNames := mockArgoCli.ListWorkflowNames()
	assert.Empty(t, wfNames, "there should no workflow created")

	// check event sink
	emittedEvent := eventSink.GetEvent()
	assert.NotNil(t, emittedEvent, "no event in the event sink")
	assert.Equal(t, types.WorkflowCreateFailedEvent, emittedEvent.EventType(), "inconsistent event type")

	wfCreateFailedEvent, ok := emittedEvent.(types.WorkflowCreateFailed)
	assert.Truef(t, ok, "cannot convert to %s, is of type %s", reflect.TypeOf(types.WorkflowCreateFailed{}), reflect.TypeOf(emittedEvent))
	assert.Equal(t, types.EventErrorType("FailToCreateWorkflow"), wfCreateFailedEvent.Error, "inconsistent error type")
	assert.Equal(t, provider, wfCreateFailedEvent.Provider, "inconsistent provider")
}

func createMissingEventIDCheck(t *testing.T, fields createFields, args createArgs) {

	provider := fields.provider
	mockArgoCli := fields.mockArgoCli
	eventSink := fields.eventSink

	// check argo workflow
	wfNames := mockArgoCli.ListWorkflowNames()
	assert.Empty(t, wfNames, "there should no workflow created")

	// check event sink
	emittedEvent := eventSink.GetEvent()
	assert.NotNil(t, emittedEvent, "no event in the event sink")
	assert.Equal(t, types.WorkflowCreateFailedEvent, emittedEvent.EventType(), "inconsistent event type")

	wfCreateFailedEvent, ok := emittedEvent.(types.WorkflowCreateFailed)
	assert.Truef(t, ok, "cannot convert to %s, is of type %s", reflect.TypeOf(types.WorkflowCreateFailed{}), reflect.TypeOf(emittedEvent))
	assert.Equal(t, types.EventErrorType("MissingEventID"), wfCreateFailedEvent.Error, "inconsistent error type")
	assert.Equal(t, provider, wfCreateFailedEvent.Provider, "inconsistent provider")
}
