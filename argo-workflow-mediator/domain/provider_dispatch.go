package domain

import (
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// ProviderDispatcher decides which provider to dispatch an event to
type ProviderDispatcher struct {
	defaultProvider types.Provider
	providerConf    map[types.Provider]config.ProviderConfig
}

// NewProviderDispatcher ...
func NewProviderDispatcher(providerConf map[types.Provider]config.ProviderConfig, defaultProvider types.Provider) ProviderDispatcher {
	return ProviderDispatcher{
		defaultProvider: defaultProvider,
		providerConf:    providerConf,
	}
}

// Dispatch ...
func (d ProviderDispatcher) Dispatch(eventProvider types.Provider) types.Provider {
	_, ok := d.providerConf[eventProvider]
	if !ok {
		return d.defaultProvider
	}
	return eventProvider
}
