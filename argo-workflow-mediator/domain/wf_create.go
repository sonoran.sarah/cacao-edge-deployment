package domain

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	wf "gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/domain/workflow"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// WorkflowCreateHandler is a msg handler for WorkflowCreate
type WorkflowCreateHandler struct {
	argoClis           map[types.Provider]ports.ArgoClient
	wfSrcs             map[types.Provider]ports.WorkflowDefSrc
	eventSink          ports.EventSink
	tfBackend          ports.TFBackend
	credStores         map[types.Provider]ports.K8SCredSecretStore
	natsOut            config.NatsConfig
	providerDispatcher ProviderDispatcher
}

// NewWorkflowCreateHandler creates a WorkflowCreateCmdHandler
func NewWorkflowCreateHandler(domain Domain) WorkflowCreateHandler {
	return WorkflowCreateHandler{
		argoClis:           domain.ArgoClis,
		wfSrcs:             domain.WfSrcs,
		eventSink:          domain.EventSink,
		tfBackend:          domain.TfBackend,
		credStores:         domain.CredStores,
		natsOut:            domain.natsOut,
		providerDispatcher: domain.providerDispatcher,
	}
}

// Handle handles the types.WorkflowCreate.
func (h WorkflowCreateHandler) Handle(event types.WorkflowCreate) error {
	var err error
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "WorkflowCreateHandler.Handle",
	})
	// overwrite the provider in the event
	event.Provider = h.providerDispatcher.Dispatch(event.Provider)

	if err = h.validateEvent(event); err != nil {
		return err
	}

	wfName, err := h.createWorkflow(event)
	if err != nil {
		h.publishFailedEvent(event, types.FailToCreateWorkflow, err.Error())
		return err
	}

	logger.WithField("workflow", wfName).Info("workflow created")

	err = h.publishCreatedEvent(event, wfName)
	if err != nil {
		return err
	}
	return nil
}

func (h WorkflowCreateHandler) validateEvent(event types.WorkflowCreate) error {
	err := h.checkEventID(event)
	if err != nil {
		h.publishFailedEvent(event, types.MissingEventID, err.Error())
		return err
	}

	err = h.checkProvider(event)
	if err != nil {
		h.publishFailedEvent(event, types.ProviderNotSupported, err.Error())
		return err
	}
	return nil
}

func (h WorkflowCreateHandler) checkEventID(event types.WorkflowCreate) error {
	if event.EventID == "" {
		return MissingEventID{event.EventType()}
	}
	return nil
}

func (h WorkflowCreateHandler) checkProvider(event types.WorkflowCreate) error {
	var ok bool
	var err error

	// check if provider is supported
	if _, ok = h.argoClis[event.Provider]; !ok {
		err = UnsupportedProvider{Provider: event.Provider, reason: "provider missing from ArgoClient/ArgoConf"}
		return err
	}
	if _, ok = h.wfSrcs[event.Provider]; !ok {
		err = UnsupportedProvider{Provider: event.Provider, reason: "provider missing from Workflow Srcs"}
		return err
	}
	return nil
}

func (h WorkflowCreateHandler) createWorkflow(event types.WorkflowCreate) (string, error) {
	wfSrc := h.wfSrcs[event.Provider]

	fac, err := wf.NewFactory(
		event.TransactionID,
		event.Provider,
		h.argoClis[event.Provider],
		wfSrc,
		event.WorkflowFilename,
		h.tfBackend,
		h.credStores[event.Provider],
		h.natsOut,
	)
	if err != nil {
		return "", err
	}
	wfName, err := fac.Create(event.WfDat)
	if err != nil {
		return "", err
	}
	return wfName, nil
}

// Publish WorkflowCreated event
func (h WorkflowCreateHandler) publishCreatedEvent(reqEvent types.WorkflowCreate, wfName string) error {
	event := types.WorkflowCreated{
		RequestID:     reqEvent.EventID,
		TransactionID: reqEvent.TransactionID,
		Provider:      reqEvent.Provider,
		WorkflowName:  wfName,
	}
	return h.eventSink.Publish(event)
}

// Publish WorkflowCreateFailed event
func (h WorkflowCreateHandler) publishFailedEvent(reqEvent types.WorkflowCreate, errType types.EventErrorType, errMsg string) error {
	event := types.WorkflowCreateFailed{
		RequestID:     reqEvent.EventID,
		TransactionID: reqEvent.Transaction(),
		Provider:      reqEvent.Provider,
		Error:         errType,
		Msg:           errMsg,
	}
	return h.eventSink.Publish(event)
}
