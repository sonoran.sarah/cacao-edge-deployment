package config

// ArgoConfig is configuration for Argo Workflow
type ArgoConfig struct {
	Host           string
	Port           uint
	SSL            bool
	SSLVerify      bool
	Namespace      string
	ServiceAccount string
	Token          string
}

// ArgoConfigFromProviderConfig extract an ArgoConfig from a ProviderConfig
func ArgoConfigFromProviderConfig(conf ProviderConfig) ArgoConfig {
	var argoConf ArgoConfig

	argoConf.Host = conf.Host
	argoConf.Port = conf.Port
	argoConf.SSL = conf.SSL
	argoConf.SSLVerify = conf.SSLVerify
	argoConf.Namespace = conf.Namespace
	argoConf.ServiceAccount = conf.ServiceAccount
	argoConf.Token = conf.Token

	return argoConf
}
