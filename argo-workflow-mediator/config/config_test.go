package config

import (
	"reflect"
	"testing"

	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

const testConfStr = `
nats_in:
    nats_url: localhost:4222
    cluster_id: stan 
    client_id: awm-
    subject: awm.Cmd
    queue: awm.Cmd
    durable_name: argo-workflow-mediator
nats_out:
    nats_url: localhost:4222
    cluster_id: stan 
    client_id: awm-
    subject: awm.Event
default_provider: provider-c1emo444k9ukjr07vob0
providers:
    provider-c1emo444k9ukjr07vob0:
        api_host: "127.0.0.1"
        api_port: 2746
        ssl_verify: false
        namespace: "argo"
        service_account: "argo-svc-account"
        token: "svc-acc-token"
        cert_auth_data: abcdABCD
        k8s_api_url: https://localhost:6443
tf_backend:
    tf_backend_type: pg
    pg_host: localhost
    pg_port: 5432
    admin_user: postgres
    admin_password: postgres-password
    user_creden_db: credential_database
    user_creden_table: user_credential_table
    tf_state_db: terraform_state_database
    tf_schema_postfix: _tf_state
    tf_workspace_postfix: _template
workflow_base_dir: "/path/to/workflow/base/dir"`

func TestParseConfigFromYAML(t *testing.T) {
	type args struct {
		dat []byte
	}

	defaultProvider := types.Provider("provider-c1emo444k9ukjr07vob0")
	providerConf := ProviderConfig{"127.0.0.1", 2746, false, false, "argo", "argo-svc-account", "svc-acc-token", "abcdABCD", "https://localhost:6443", false}
	natsInConfig := NatsConfig{"localhost:4222", "stan", "awm.Cmd", "awm.Cmd", "argo-workflow-mediator"}
	natsOutConfig := NatsConfig{"localhost:4222", "stan", "awm.Event", "", ""}
	tfBackendConfig := TFBackendConfig{
		"pg",
		TFPostgresBackendConfig{"localhost", 5432, "postgres", "postgres-password", "credential_database", "user_credential_table", "terraform_state_database", "_tf_state", "_template"},
		TFHTTPBackendConfig{},
	}

	tests := []struct {
		name    string
		args    args
		want    *Config
		wantErr bool
	}{
		{"normal", args{[]byte(testConfStr)}, &Config{map[types.Provider]ProviderConfig{defaultProvider: providerConf}, defaultProvider, natsInConfig, natsOutConfig, tfBackendConfig, "/path/to/workflow/base/dir"}, false},
		{"empty", args{[]byte("")}, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseConfigFromYAML(tt.args.dat)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseConfigFromYAML() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseConfigFromYAML() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestValidateProviderConfig(t *testing.T) {
	testProvider := types.Provider("provider-" + xid.New().String())
	type args struct {
		provider types.Provider
		conf     *ProviderConfig
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"normal", args{testProvider, &ProviderConfig{"host", 1234, false, false, "namespace", "svc-account", "token", "abcdABCD", "https://localhost:6443", false}}, false},
		{"ssl", args{testProvider, &ProviderConfig{"host", 1234, true, true, "namespace", "svc-account", "token", "abcdABCD", "https://localhost:6443", false}}, false},
		{"missing host", args{testProvider, &ProviderConfig{"", 1234, true, false, "namespace", "svc-account", "token", "abcdABCD", "https://localhost:6443", false}}, true},
		{"missing namespace", args{testProvider, &ProviderConfig{"host", 1234, true, false, "", "svc-account", "token", "abcdABCD", "https://localhost:6443", false}}, true},
		{"missing svc account", args{testProvider, &ProviderConfig{"host", 1234, true, false, "namespace", "", "token", "abcdABCD", "https://localhost:6443", false}}, true},
		{"missing token", args{testProvider, &ProviderConfig{"host", 1234, true, false, "namespace", "svc-account", "", "abcdABCD", "https://localhost:6443", false}}, true},
		{"missing cert auth data", args{testProvider, &ProviderConfig{"host", 1234, true, false, "namespace", "svc-account", "token", "", "https://localhost:6443", false}}, true},
		{"missing k8s api url", args{testProvider, &ProviderConfig{"host", 1234, true, false, "namespace", "svc-account", "token", "abcdABCD", "", false}}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validateProviderConfig(tt.args.provider, tt.args.conf); (err != nil) != tt.wantErr {
				t.Errorf("validateProviderConfig() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestValidateConfig(t *testing.T) {
	type args struct {
		conf *Config
	}

	defaultProvider := types.Provider("provider-" + xid.New().String())
	testProvider := types.Provider("provider-" + xid.New().String())
	providerConf := ProviderConfig{"host", 1234, false, false, "namespace", "svc-account", "token", "abcdABCD", "https://localhost:6443", false}
	emptyProviderConf := ProviderConfig{}
	natsConfig := NatsConfig{"URL", "cluster-id", "root.subject", "queue", "durable-name"}
	tfBackendConfig := TFBackendConfig{
		"pg",
		TFPostgresBackendConfig{"host", 1234, "admin_user", "admin_password", "user_cred_db", "user_cred_table", "tf_state_db", "tf_state_schema_postfix", "tf_workspace_name_postfix"},
		TFHTTPBackendConfig{},
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"normal", args{&Config{map[types.Provider]ProviderConfig{defaultProvider: providerConf}, defaultProvider, natsConfig, natsConfig, tfBackendConfig, "/base/dir"}}, false},
		{"missing default provider config", args{&Config{map[types.Provider]ProviderConfig{}, defaultProvider, natsConfig, natsConfig, tfBackendConfig, "/base/dir"}}, true},
		{"bad nats config - in", args{&Config{map[types.Provider]ProviderConfig{defaultProvider: providerConf}, defaultProvider, NatsConfig{}, natsConfig, tfBackendConfig, "/base/dir"}}, true},
		{"bad nats config - out", args{&Config{map[types.Provider]ProviderConfig{defaultProvider: providerConf}, defaultProvider, natsConfig, NatsConfig{}, tfBackendConfig, "/base/dir"}}, true},
		{"bad TF config", args{&Config{map[types.Provider]ProviderConfig{defaultProvider: providerConf}, defaultProvider, natsConfig, natsConfig, TFBackendConfig{}, "/base/dir"}}, true},
		{"bad provider config", args{&Config{map[types.Provider]ProviderConfig{defaultProvider: providerConf, testProvider: emptyProviderConf}, defaultProvider, natsConfig, natsConfig, tfBackendConfig, "/base/dir"}}, true},
		{"missing workflow base dir", args{&Config{map[types.Provider]ProviderConfig{defaultProvider: providerConf}, defaultProvider, natsConfig, natsConfig, tfBackendConfig, ""}}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validateConfig(tt.args.conf); (err != nil) != tt.wantErr {
				t.Errorf("validateConfig() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
