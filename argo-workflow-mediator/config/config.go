package config

import (
	"io/ioutil"

	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
	"gopkg.in/yaml.v2"
)

// EnvConfig contains config from environment variables
type EnvConfig struct {
	// Log level of the global logger.
	// Check logrus.ParseLevel() for string value for different log level.
	LogLevel string `envconfig:"LOG_LEVEL" default:"trace"`
	// Path to the Config file
	ConfigPath string `envconfig:"AWM_CONFIG" default:"config.yml"`
	// Transport Encryption key for cloud credential, encoded in base64
	CredEncryptKeyBase64 string `envconfig:"AWM_CRED_KEY"`
	// Name of the pod that this instance of Argo Workflow Mediator runs in
	PodName string `envconfig:"POD_NAME"`
	// number of event worker spawned
	EventWorkerCount uint `envconfig:"EVENT_WORKER_COUNT" default:"1"`
	// buffer capacity of the go channel for events
	EventChannelBufferCapacity uint `envconfig:"EVENT_CHAN_BUFFER_CAP" default:"5"`
	// encode k8s secret name (for user credentials) in hex encoding, this is to avoid invalid character in the k8s secret name. e.g. '@'
	HexEncodeSecretName bool `envconfig:"hex_encode_secret_name" default:"false"`
}

// Config contains all configurations for Argo Workflow Mediator
type Config struct {
	ProviderConf    map[types.Provider]ProviderConfig `yaml:"providers"`
	DefaultProvider types.Provider                    `yaml:"default_provider"`

	// NATS Config for input/subscribing messages
	NatsInConf NatsConfig `yaml:"nats_in"`

	// NATS Config for output/publishing messages
	NatsOutConf NatsConfig `yaml:"nats_out"`

	TFBackend TFBackendConfig `yaml:"tf_backend"`
	// Base directory for all workflow definition files
	WorkflowBaseDir string `yaml:"workflow_base_dir"`
}

// ProviderConfig is provider specific config
type ProviderConfig struct {
	Host            string `yaml:"api_host"`
	Port            uint   `yaml:"api_port"`
	SSL             bool   `yaml:"ssl"`
	SSLVerify       bool   `yaml:"ssl_verify"`
	Namespace       string `yaml:"namespace"`
	ServiceAccount  string `yaml:"service_account"`
	Token           string `yaml:"token"`
	CertAuthData    string `yaml:"cert_auth_data"`
	K8SAPIServerURL string `yaml:"k8s_api_url"`
	K8SInCluster    bool   `yaml:"k8s_in_cluster"`
}

// LoadConfigFromFile reads configuration from a yaml file
func LoadConfigFromFile(path string) (*Config, error) {
	dat, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	return ParseConfigFromYAML(dat)
}

// ParseConfigFromYAML parse config from serialized yaml
func ParseConfigFromYAML(dat []byte) (*Config, error) {
	var conf Config
	err := yaml.Unmarshal(dat, &conf)
	if err != nil {
		return nil, err
	}

	err = validateConfig(&conf)
	if err != nil {
		return nil, err
	}

	return &conf, nil
}

func validateConfig(conf *Config) error {
	var err error
	_, ok := conf.ProviderConf[conf.DefaultProvider]
	if !ok {
		return DefaultProviderMissing{}
	}
	if err = validateNATSConfig(&conf.NatsInConf); err != nil {
		return err
	}
	if err = validateNATSConfig(&conf.NatsOutConf); err != nil {
		return err
	}
	for provider, providerConf := range conf.ProviderConf {
		err = validateProviderConfig(provider, &providerConf)
		if err != nil {
			return err
		}
	}
	if err = validateTFBackendConfig(&conf.TFBackend); err != nil {
		return err
	}
	if conf.WorkflowBaseDir == "" {
		return WorkflowBaseDirMissing{}
	}
	return nil
}

func validateProviderConfig(provider types.Provider, conf *ProviderConfig) error {
	if conf.Host == "" {
		return ProviderConfigMissingField{provider: provider, field: "api_host"}
	}
	if conf.Port == 0 {
		return ProviderConfigMissingField{provider: provider, field: "api_port"}
	}
	if conf.Namespace == "" {
		return ProviderConfigMissingField{provider: provider, field: "namespace"}
	}
	if conf.ServiceAccount == "" {
		return ProviderConfigMissingField{provider: provider, field: "service_account"}
	}
	if conf.Token == "" {
		return ProviderConfigMissingField{provider: provider, field: "token"}
	}
	if conf.CertAuthData == "" {
		return ProviderConfigMissingField{provider: provider, field: "cert_auth_data"}
	}
	if conf.K8SAPIServerURL == "" {
		return ProviderConfigMissingField{provider: provider, field: "k8s_api_url"}
	}
	return nil
}

// DefaultConfig returns the provider config of the default provider
func (conf Config) DefaultConfig() *ProviderConfig {
	providerConf, ok := conf.ProviderConf[conf.DefaultProvider]
	if !ok {
		return nil
	}
	return &providerConf
}

// ProviderConfig returns provider config of given provider
// return nil if given provider not found
func (conf Config) ProviderConfig(provider types.Provider) *ProviderConfig {
	providerConf, ok := conf.ProviderConf[provider]
	if !ok {
		return nil
	}
	return &providerConf
}
