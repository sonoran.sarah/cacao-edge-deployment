package config

import (
	"testing"
)

func TestValidateTFConfig(t *testing.T) {
	type args struct {
		conf *TFBackendConfig
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"normal pg", args{&TFBackendConfig{
			"pg",
			TFPostgresBackendConfig{"host", 1234, "admin_user", "admin_password", "user_cred_db", "user_cred_table", "tf_state_db", "tf_state_schema_postfix", "tf_workspace_name_postfix"},
			TFHTTPBackendConfig{},
		}}, false},
		{"empty backend type", args{&TFBackendConfig{
			"",
			TFPostgresBackendConfig{"host", 1234, "admin_user", "admin_password", "user_cred_db", "user_cred_table", "tf_state_db", "tf_state_schema_postfix", "tf_workspace_name_postfix"},
			TFHTTPBackendConfig{},
		}}, false},
		{"missing Host", args{&TFBackendConfig{
			"pg",
			TFPostgresBackendConfig{"", 1234, "admin_user", "admin_password", "user_cred_db", "user_cred_table", "tf_state_db", "tf_state_schema_postfix", "tf_workspace_name_postfix"},
			TFHTTPBackendConfig{},
		}}, true},
		{"missing Port", args{&TFBackendConfig{
			"pg",
			TFPostgresBackendConfig{"host", 0, "admin_user", "admin_password", "user_cred_db", "user_cred_table", "tf_state_db", "tf_state_schema_postfix", "tf_workspace_name_postfix"},
			TFHTTPBackendConfig{},
		}}, true},
		{"small Port", args{&TFBackendConfig{
			"pg",
			TFPostgresBackendConfig{"host", 80, "admin_user", "admin_password", "user_cred_db", "user_cred_table", "tf_state_db", "tf_state_schema_postfix", "tf_workspace_name_postfix"},
			TFHTTPBackendConfig{},
		}}, false},
		{"large Port", args{&TFBackendConfig{
			"pg",
			TFPostgresBackendConfig{"host", 65536, "admin_user", "admin_password", "user_cred_db", "user_cred_table", "tf_state_db", "tf_state_schema_postfix", "tf_workspace_name_postfix"},
			TFHTTPBackendConfig{},
		}}, true},
		{"missing AdminUser", args{&TFBackendConfig{
			"pg",
			TFPostgresBackendConfig{"host", 1234, "", "admin_password", "user_cred_db", "user_cred_table", "tf_state_db", "tf_state_schema_postfix", "tf_workspace_name_postfix"},
			TFHTTPBackendConfig{},
		}}, true},
		{"missing AdminPassword", args{&TFBackendConfig{
			"pg",
			TFPostgresBackendConfig{"host", 1234, "admin_user", "", "user_cred_db", "user_cred_table", "tf_state_db", "tf_state_schema_postfix", "tf_workspace_name_postfix"},
			TFHTTPBackendConfig{},
		}}, true},
		{"missing UserCredentialDB", args{&TFBackendConfig{
			"pg",
			TFPostgresBackendConfig{"host", 1234, "admin_user", "admin_password", "", "user_cred_table", "tf_state_db", "tf_state_schema_postfix", "tf_workspace_name_postfix"},
			TFHTTPBackendConfig{},
		}}, true},
		{"missing UserCredentialTable", args{&TFBackendConfig{
			"pg",
			TFPostgresBackendConfig{"host", 1234, "admin_user", "admin_password", "user_cred_db", "", "tf_state_db", "tf_state_schema_postfix", "tf_workspace_name_postfix"},
			TFHTTPBackendConfig{},
		}}, true},
		{"missing TFStateDB", args{&TFBackendConfig{
			"pg",
			TFPostgresBackendConfig{"host", 1234, "admin_user", "admin_password", "user_cred_db", "user_cred_table", "", "tf_state_schema_postfix", "tf_workspace_name_postfix"},
			TFHTTPBackendConfig{},
		}}, true},
		{"missing TFStateSchemaNamePostfix", args{&TFBackendConfig{
			"pg",
			TFPostgresBackendConfig{"host", 1234, "admin_user", "admin_password", "user_cred_db", "user_cred_table", "tf_state_db", "", "tf_workspace_name_postfix"},
			TFHTTPBackendConfig{},
		}}, true},
		{"missing TFWorkspaceNamePostfix", args{&TFBackendConfig{
			"pg",
			TFPostgresBackendConfig{"host", 1234, "admin_user", "admin_password", "user_cred_db", "user_cred_table", "tf_state_db", "tf_state_schema_postfix", ""},
			TFHTTPBackendConfig{},
		}}, true},
		{"normal http", args{&TFBackendConfig{
			"http",
			TFPostgresBackendConfig{},
			TFHTTPBackendConfig{HTTPBaseURL: "http://localhost", HTTPJWTSecretKey: "1234567890123456789012345678901234567890"},
		}}, false},
		{"http missing url", args{&TFBackendConfig{
			"http",
			TFPostgresBackendConfig{},
			TFHTTPBackendConfig{HTTPBaseURL: "", HTTPJWTSecretKey: "1234567890123456789012345678901234567890"},
		}}, true},
		{"http missing jwt key", args{&TFBackendConfig{
			"http",
			TFPostgresBackendConfig{},
			TFHTTPBackendConfig{HTTPBaseURL: "http://localhost", HTTPJWTSecretKey: ""},
		}}, true},
		{"http short jwt key", args{&TFBackendConfig{
			"http",
			TFPostgresBackendConfig{},
			TFHTTPBackendConfig{HTTPBaseURL: "http://localhost", HTTPJWTSecretKey: "12345"},
		}}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validateTFBackendConfig(tt.args.conf); (err != nil) != tt.wantErr {
				t.Errorf("validateTFBackendConfig() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
