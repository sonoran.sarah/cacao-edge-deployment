package config

// WorkflowConfig is config for workflow definition files
type WorkflowConfig struct {
	WorkflowBaseDir string `yaml:"workflow_base_dir"`
}
