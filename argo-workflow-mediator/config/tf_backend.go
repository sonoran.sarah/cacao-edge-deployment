package config

import (
	"errors"
	"fmt"
	"regexp"

	log "github.com/sirupsen/logrus"
)

// TFBackendConfig is config for Terraform Backend
type TFBackendConfig struct {
	TFBackendType           string `yaml:"tf_backend_type"`
	TFPostgresBackendConfig `yaml:",inline"`
	TFHTTPBackendConfig     `yaml:",inline"`
}

const (
	// TFPGBackend postgres backend
	TFPGBackend = "pg"
	// TFHTTPBackend http backend
	TFHTTPBackend = "http"
)

// TFPostgresBackendConfig config for Terraform Postgres backend
type TFPostgresBackendConfig struct {
	Host                     string `yaml:"pg_host"`
	Port                     uint   `yaml:"pg_port"`
	AdminUser                string `yaml:"admin_user"`
	AdminPassword            string `yaml:"admin_password"`
	UserCredentialDB         string `yaml:"user_creden_db"`
	UserCredentialTable      string `yaml:"user_creden_table"`
	TFStateDB                string `yaml:"tf_state_db"`
	TFStateSchemaNamePostfix string `yaml:"tf_schema_postfix"`
	TFWorkspaceNamePostfix   string `yaml:"tf_workspace_postfix"`
}

// TFHTTPBackendConfig config for Terraform HTTP backend
type TFHTTPBackendConfig struct {
	// HTTP backend URL
	HTTPBaseURL string `yaml:"http_base_url"`
	// JWT token secret for HTTP backend
	HTTPJWTSecretKey string `yaml:"http_jwt_secret_key"`
}

func validateTFBackendConfig(conf *TFBackendConfig) error {
	if conf.TFBackendType == "" || conf.TFBackendType == TFPGBackend {
		return validateTFPostgresBackendConfig(conf)
	} else if conf.TFBackendType == TFHTTPBackend {
		return validateTFHTTPBackendConfig(conf)
	}
	return fmt.Errorf("unknown terraform backend type, %s", conf.TFBackendType)
}

func validateTFPostgresBackendConfig(conf *TFBackendConfig) error {
	if conf.Host == "" {
		return TFBackendConfigMissingField{field: "pg_host"}
	}
	if conf.Port == 0 {
		return TFBackendConfigMissingField{field: "pg_port"}
	}
	if conf.Port <= 1024 {
		log.Warn("Postgres uses a low port number ", conf.Port)
	}
	if conf.Port > 65535 {
		return TFBackendConfigBadValue{field: "pg_port", reason: "Postgres port > 65535"}
	}
	if conf.AdminUser == "" {
		return TFBackendConfigMissingField{field: "admin_user"}
	}
	if conf.AdminPassword == "" {
		return TFBackendConfigMissingField{field: "admin_password"}
	}
	if conf.UserCredentialDB == "" {
		return TFBackendConfigMissingField{field: "user_creden_db"}
	}
	err := tfBackendCheckField(conf.UserCredentialDB, "", "user_creden_db", "DB name contains invalid char")
	if err != nil {
		return err
	}
	if conf.UserCredentialTable == "" {
		return TFBackendConfigMissingField{field: "user_creden_table"}
	}
	err = tfBackendCheckField(conf.UserCredentialTable, "", "user_creden_table", "table name contains invalid char")
	if err != nil {
		return err
	}
	if conf.TFStateDB == "" {
		return TFBackendConfigMissingField{field: "tf_state_db"}
	}
	err = tfBackendCheckField(conf.TFStateDB, "", "tf_state_db", "DB name contains invalid char")
	if err != nil {
		return err
	}
	if conf.TFStateSchemaNamePostfix == "" {
		return TFBackendConfigMissingField{field: "tf_schema_postfix"}
	}
	err = tfBackendCheckField(conf.TFStateSchemaNamePostfix, "^[a-zA-Z_][a-zA-Z0-9_]*$", "tf_schema_postfix", "postfix contains invalid char")
	if err != nil {
		return err
	}
	if conf.TFWorkspaceNamePostfix == "" {
		return TFBackendConfigMissingField{field: "tf_workspace_postfix"}
	}
	err = tfBackendCheckField(conf.TFWorkspaceNamePostfix, "^[a-zA-Z_][a-zA-Z0-9_]*$", "tf_workspace_postfix", "postfix contains invalid char")
	if err != nil {
		return err
	}

	return nil
}

func validateTFHTTPBackendConfig(conf *TFBackendConfig) error {
	if conf.HTTPBaseURL == "" {
		return errors.New("URL for terraform http backend is empty")
	}
	if len(conf.HTTPJWTSecretKey) < 32 {
		return errors.New("jwt secret key length < 32")
	}
	return nil
}

func tfBackendCheckField(value string, regex string, field string, reason string) error {
	if regex == "" {
		regex = "^[a-zA-Z][a-zA-Z0-9_]*$"
	}
	matched, err := regexp.Match(regex, []byte(value))
	if err != nil {
		return err
	}
	if !matched {
		return TFBackendConfigBadValue{field, reason}
	}
	return nil
}
