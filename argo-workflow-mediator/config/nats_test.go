package config

import (
	"testing"
)

func TestValidateNatsConfig(t *testing.T) {
	type args struct {
		conf *NatsConfig
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"normal", args{&NatsConfig{"URL", "cluster-id", "root.subject", "queue", "durable-name"}}, false},
		{"missing URL", args{&NatsConfig{"", "cluster-id", "root.subject", "queue", "durable-name"}}, true},
		{"missing ClusterID", args{&NatsConfig{"URL", "", "root.subject", "queue", "durable-name"}}, true},
		{"missing Subject", args{&NatsConfig{"URL", "cluster-id", "", "queue", "durable-name"}}, true},
		{"missing Queue", args{&NatsConfig{"URL", "cluster-id", "root.subject", "", "durable-name"}}, false},
		{"missing DurableName", args{&NatsConfig{"URL", "cluster-id", "root.subject", "queue", ""}}, false},
		{"subject no dot", args{&NatsConfig{"URL", "cluster-id", "subject", "queue", "durable-name"}}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validateNATSConfig(tt.args.conf); (err != nil) != tt.wantErr {
				t.Errorf("validateNATSConfig() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
