package config

import (
	"strings"

	log "github.com/sirupsen/logrus"
)

// NatsConfig is config for NATS Streaming Connection
type NatsConfig struct {
	NatsURL     string `yaml:"nats_url"`
	ClusterID   string `yaml:"cluster_id"`
	Subject     string `yaml:"subject"`
	Queue       string `yaml:"queue"`
	DurableName string `yaml:"durable_name"`
}

func validateNATSConfig(conf *NatsConfig) error {
	if conf.ClusterID == "" {
		return NatsConfigMissingField{"cluster_id"}
	}
	if conf.NatsURL == "" {
		return NatsConfigMissingField{"nats_url"}
	}
	if conf.Subject == "" {
		return NatsConfigMissingField{"subject"}
	} else if !strings.ContainsAny(conf.Subject, ".") {
		return NatsConfigBadValue{"subject", "contain no '.'"}
	}

	if conf.DurableName == "" {
		log.Warn("NatsConf missing durable_name")
	}
	if conf.Queue == "" {
		log.Warn("NatsConf missing queue")
	}

	return nil
}
