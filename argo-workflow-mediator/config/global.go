package config

import (
	"log"
)

// global config
var globalConfig *Config

// GlobalConfig returns the global configuration
func GlobalConfig() *Config {
	return globalConfig
}

// LoadGlobalConfigFromFile loads the global config from file
func LoadGlobalConfigFromFile(path string) error {
	config, err := LoadConfigFromFile(path)
	if err != nil {
		log.Fatalln(err)
	}
	globalConfig = config
	return nil
}

// LoadGlobalConfig loads config as global configuration
func LoadGlobalConfig(config *Config) {
	globalConfig = config
}
