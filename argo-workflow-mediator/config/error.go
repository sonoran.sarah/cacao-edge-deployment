package config

import (
	"fmt"

	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// ConfFileError is error occurs when reading argo config file
type ConfFileError struct {
	msg string
}

func (err ConfFileError) Error() string {
	return err.msg
}

// DefaultProviderMissing indicates default provider or its config is missing
type DefaultProviderMissing struct {
}

func (err DefaultProviderMissing) Error() string {
	return "Default provider is missing from config"
}

// ProviderConfigMissingField indicates there is missing field in ProviderConfig
type ProviderConfigMissingField struct {
	provider types.Provider
	field    string
}

func (err ProviderConfigMissingField) Error() string {
	return fmt.Sprintf("Missing %s field from %s provider config", err.field, err.provider)
}

// NatsConfigMissingField indicates there is missing field in NatsConfig
type NatsConfigMissingField struct {
	field string
}

func (err NatsConfigMissingField) Error() string {
	return fmt.Sprintf("Missing field %s from nats config", err.field)
}

// NatsConfigBadValue indicates a field in NatsConfig has bad value
type NatsConfigBadValue struct {
	field  string
	reason string
}

func (err NatsConfigBadValue) Error() string {
	return fmt.Sprintf("Field %s has bad value from nats config, %s", err.field, err.reason)
}

// TFBackendConfigMissingField indicates there is missing field in TFBackendConfig
type TFBackendConfigMissingField struct {
	field string
}

func (err TFBackendConfigMissingField) Error() string {
	return fmt.Sprintf("Missing field %s from terraform backend config", err.field)
}

// TFBackendConfigBadValue indicates a field in TFBackendConfig has bad value
type TFBackendConfigBadValue struct {
	field  string
	reason string
}

func (err TFBackendConfigBadValue) Error() string {
	return fmt.Sprintf("Field %s has bad value from terraform backend config, %s", err.field, err.reason)
}

// WorkflowBaseDirMissing indicates WorkflowBaseDir is missing
type WorkflowBaseDirMissing struct {
}

func (err WorkflowBaseDirMissing) Error() string {
	return "WorkflowBaseDir is missing from config"
}
