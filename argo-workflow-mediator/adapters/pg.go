package adapters

import (
	"crypto/rand"
	"database/sql"
	"encoding/base64"
	"fmt"
	"strings"

	_ "github.com/lib/pq" // postgresql driver
	log "github.com/sirupsen/logrus"
)

// PostgresCreden is credential to Postgresql
type PostgresCreden struct {
	Host     string `yaml:"host"`
	Port     uint   `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
}

func (creden PostgresCreden) connStr(dbname string) string {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		creden.Host, creden.Port, creden.User, creden.Password, dbname)
	return psqlInfo
}

// Get the backend block in HCL for user
// e.g.
// backend "pg" {
//   conn_str = "postgres://username:password@postgresql_host:5432/database_name"
// }
func (creden PostgresCreden) getHCLBackendBlock(dbname string, schemaName string) string {
	backendBlock := `
backend "pg" {
  conn_str = "postgres://%s:%s@%s:%d/%s?sslmode=disable"
  schema_name = "%s"
  skip_schema_creation = true
  skip_table_creation = true
  skip_index_creation = true
}`
	backendBlock = fmt.Sprintf(backendBlock, creden.User, creden.Password, creden.Host, creden.Port, dbname, schemaName)

	return backendBlock
}

// Postgres is an interface for Postgres Database
type Postgres interface {
	Connect(creden PostgresCreden, dbname string) error
	FindDB(dbname string) bool
	FindTable(tableName string) bool
	CreateDB(dbname string, owner string) error
	CreateSQLUser(username string, password string) error
	GrantUsageOnSchema(schemaName string, sqlUser string) error
	Exec(query string) error
	Prep(query string) (PgStmt, error)
}

// PgStmt is an interface to sql.Stmt returned by Postgres interface
type PgStmt interface {
	DoQuery(args ...interface{}) (PgRows, error)
	Exec(args ...interface{}) (sql.Result, error)
	Close() error
}

type PgRows interface {
	Next() bool
	Scan(dest ...interface{}) error
	Err() error
	Close() error
}

// pgStmt implements PgStmt
type pgStmt struct {
	*sql.Stmt
}

// DoQuery ...
func (p pgStmt) DoQuery(args ...interface{}) (PgRows, error) {
	return p.Query(args...)
}

// PostgresDB implements Postgres
type PostgresDB struct {
	conn *sql.DB
}

// NewPostgres ...
func NewPostgres() Postgres {
	return &PostgresDB{}
}

// Connect connects to the specfied database with the given credential
func (pg *PostgresDB) Connect(creden PostgresCreden, dbname string) error {
	psqlInfo := creden.connStr(dbname)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return err
	}

	// Ping to establish connection
	err = db.Ping()
	if err != nil {
		defer db.Close()
		return err
	}

	pg.conn = db
	return nil
}

// FindDB checks if a database with the specified name exists
func (pg PostgresDB) FindDB(dbname string) bool {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "PostgresDB.FindDB",
	})
	query := `SELECT datname FROM pg_database WHERE datistemplate=false AND datname=$1;`

	stmt, err := pg.conn.Prepare(query)
	if err != nil {
		logger.WithError(err).Error("fail to prepare query")
		return false
	}
	defer stmt.Close()

	rows, err := stmt.Query(dbname)
	if err != nil {
		logger.WithError(err).Error("fail to exec query")
		return false
	}

	dbnames := make([]string, 0)
	for rows.Next() {
		var name string
		if err := rows.Scan(&name); err != nil {
			return false
		}
		dbnames = append(dbnames, name)
	}
	// Check for errors from iterating over rows.
	if err := rows.Err(); err != nil {
		logger.WithError(err).Error("error in rows")
		return false
	}

	return len(dbnames) > 0
}

// FindTable checks if there exists a table with the specified name
func (pg PostgresDB) FindTable(tableName string) bool {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "PostgresDB.FindTable",
	})
	query := `SELECT table_name
  FROM information_schema.tables
 WHERE table_schema='public'
   AND table_type='BASE TABLE'
   AND table_name=$1;`

	stmt, err := pg.conn.Prepare(query)
	if err != nil {
		logger.WithError(err).Error("fail to prepare query")
		return false
	}
	defer stmt.Close()

	rows, err := stmt.Query(tableName)
	if err != nil {
		logger.WithError(err).Error("fail to exec query")
		return false
	}

	tables := make([]string, 0)
	for rows.Next() {
		var name string
		if err := rows.Scan(&name); err != nil {
			return false
		}
		tables = append(tables, name)
	}
	// Check for errors from iterating over rows.
	if err := rows.Err(); err != nil {
		logger.WithError(err).Error("error in rows")
		return false
	}

	return len(tables) > 0
}

// CreateDB creates a database with specified name, and makes the specified
// user the owner of the database
func (pg PostgresDB) CreateDB(dbname string, owner string) error {
	query := "CREATE DATABASE %s with owner %s"
	query = fmt.Sprintf(query, dbname, owner)

	_, err := pg.conn.Query(query)
	if err != nil {
		return err
	}
	return nil
}

// CreateSQLUser creates an Postgres role with the given username and password
func (pg PostgresDB) CreateSQLUser(username string, password string) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "PostgresDB.CreateSQLUser",
		"username": username,
	})
	logger.Warn("Use simple string replacment in SQL Query, be cautious regarding SQL injection")
	query := fmt.Sprintf("CREATE USER %s WITH PASSWORD '%s';", username, password)
	_, err := pg.conn.Query(query)
	if err != nil {
		logger.Println(err)
		return err
	}
	logger.Info("pg user created")
	return nil
}

// GrantUsageOnSchema grants USAGE permission on schema to user
func (pg PostgresDB) GrantUsageOnSchema(schemaName string, sqlUser string) error {
	log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "PostgresDB.GrantUsageOnSchema",
	}).Warn("Use simple string replacment in SQL Query, be cautious regarding SQL injection")

	query := fmt.Sprintf("GRANT USAGE ON SCHEMA %s TO %s;", schemaName, sqlUser)
	_, err := pg.conn.Query(query)
	if err != nil {
		return err
	}

	query = fmt.Sprintf("GRANT USAGE ON ALL SEQUENCES IN SCHEMA %s TO %s;", schemaName, sqlUser)
	_, err = pg.conn.Query(query)
	if err != nil {
		return err
	}

	query = fmt.Sprintf("GRANT SELECT, UPDATE, INSERT ON %s.states TO %s;", schemaName, sqlUser)
	_, err = pg.conn.Query(query)
	return err
}

func (pg PostgresDB) Exec(query string) error {
	_, err := pg.conn.Query(query)
	if err != nil {
		return err
	}
	return nil
}

// Prep prepares a SQL query
func (pg PostgresDB) Prep(query string) (PgStmt, error) {
	stmt, err := pg.conn.Prepare(query)
	if err != nil {
		return nil, err
	}

	wrapper := pgStmt{
		Stmt: stmt,
	}
	return wrapper, nil
}

// Query exec a query stmt
func (pg PostgresDB) Query(stmt *sql.Stmt, args ...interface{}) (*sql.Rows, error) {
	return stmt.Query(args)
}

func (pg PostgresDB) CloseStmt(stmt *sql.Stmt) error {
	return stmt.Close()
}

func (pg PostgresDB) connection() *sql.DB {
	return pg.conn
}

// randomly generate a password
func generatePostgresPassword() string {
	length := 32
	b := make([]byte, length)
	_, err := rand.Read(b)
	if err != nil {
		log.WithFields(log.Fields{
			"package":  "adapters",
			"function": "generatePostgresPassword",
		}).WithError(err).Error("fail to read from rand")
		return ""
	}

	return strings.ReplaceAll(base64.StdEncoding.EncodeToString(b), "/", "_")
}
