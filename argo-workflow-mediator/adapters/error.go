package adapters

import (
	"fmt"
)

// UserCredenlNotFound indicates the credential for a user does not exist
type UserCredenlNotFound struct {
	Username string
}

func (err UserCredenlNotFound) Error() string {
	return fmt.Sprintf("user %s credential not found", err.Username)
}
