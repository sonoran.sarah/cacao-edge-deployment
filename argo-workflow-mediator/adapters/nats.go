package adapters

// NATSInfo is the config required to make a NATS/STAN connection
type NATSInfo struct {
	// URL to connect to NATS
	URL string

	// Cluster ID
	ClusterID string

	// Client ID
	ClientID string
}
