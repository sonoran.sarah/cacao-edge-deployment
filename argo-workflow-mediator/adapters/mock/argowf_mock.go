package mock

import (
	"fmt"

	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"

	//"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
)

// ArgoClient is a in-mem mock-up of ports.ArgoClient
type ArgoClient struct {
	workflows map[string]*wfv1.Workflow
}

// NewMockArgoClient creates a MockArgoClient
func NewMockArgoClient() ArgoClient {
	return ArgoClient{workflows: make(map[string]*wfv1.Workflow)}
}

// Init implements ports.ArgoClient
func (cli ArgoClient) Init(conf config.ArgoConfig) {
}

// Context returns an empty context
func (cli ArgoClient) Context() *ports.ArgoContext {
	return &ports.ArgoContext{}
}

// CreateWorkflow "creates" an workflow in memory from the given definition,
// and generate a name for the workflow
func (cli ArgoClient) CreateWorkflow(wfDef *wfv1.Workflow) (*wfv1.Workflow, error) {
	wfName := fmt.Sprintf("%s-%d", wfDef.Name, len(cli.workflows))
	return cli.CreateWorkflowWithName(wfName, wfDef)
}

// CreateWorkflowWithName "creates" an workflow in memory from the given definition,
// and replace the name for the workflow
func (cli ArgoClient) CreateWorkflowWithName(wfName string, wfDef *wfv1.Workflow) (*wfv1.Workflow, error) {
	cli.workflows[wfName] = wfDef.DeepCopy()
	cli.workflows[wfName].Name = wfName

	return cli.workflows[wfName], nil
}

// TerminateWorkflow ...
func (cli ArgoClient) TerminateWorkflow(wfName string) error {
	_, ok := cli.workflows[wfName]

	if !ok {
		return WorkflowNotFound{WfName: wfName}
	}
	status := wfv1.WorkflowStatus{}
	status.Phase = wfv1.WorkflowFailed

	return nil
}

// DeleteWorkflow ...
func (cli ArgoClient) DeleteWorkflow(wfName string) error {
	panic("Unimplemented")
}

// ResubmitWorkflow ...
func (cli ArgoClient) ResubmitWorkflow(wfName string) (*wfv1.Workflow, error) {
	wfDef, err := cli.GetWorkflow(wfName)
	if err != nil {
		return nil, err
	}
	newWfDef := wfDef.DeepCopy()

	return cli.CreateWorkflow(newWfDef)
}

// WorkflowStatus returns the status of the workflow that has same name as
// given, if no workflow matches the name, then returns error
func (cli ArgoClient) WorkflowStatus(wfName string) (*wfv1.WorkflowStatus, error) {
	_, ok := cli.workflows[wfName]

	if !ok {
		return nil, WorkflowNotFound{WfName: wfName}
	}
	status := wfv1.WorkflowStatus{}
	status.Phase = wfv1.WorkflowSucceeded

	return &status, nil
}

// GetWorkflow returns the definition of the workflow with a given name
func (cli ArgoClient) GetWorkflow(wfName string) (*wfv1.Workflow, error) {
	wf, ok := cli.workflows[wfName]

	if !ok {
		return nil, WorkflowNotFound{WfName: wfName}
	}

	return wf, nil
}

// ListWorkflowNames lists the names of all the workflows
func (cli ArgoClient) ListWorkflowNames() []string {
	wfNames := make([]string, 0, len(cli.workflows))
	for name := range cli.workflows {
		wfNames = append(wfNames, name)
	}
	return wfNames
}

// WorkflowNotFound indicates that the mock Workflow of a given name does not exist
type WorkflowNotFound struct {
	WfName string
}

// Error string
func (err WorkflowNotFound) Error() string {
	return fmt.Sprintf("workflow %s not found", err.WfName)
}
