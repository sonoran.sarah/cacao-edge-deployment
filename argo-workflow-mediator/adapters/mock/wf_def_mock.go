// Package mock ...
// Mock Workflow Definition Source
package mock

import (
	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
)

// WorkflowDefSrc is a in-mem mock-up Workflow Definition Source
type WorkflowDefSrc struct {
	defs map[string]wfv1.Workflow
}

// NewMockWorkflowDefSrc creates a MockWorkflowDefSrc
func NewMockWorkflowDefSrc() *WorkflowDefSrc {
	src := WorkflowDefSrc{}
	src.defs = make(map[string]wfv1.Workflow)
	return &src
}

// Init implements ports.WorkflowDefSrc, and initialize this adapter
func (src *WorkflowDefSrc) Init(conf config.WorkflowConfig) error {
	return nil
}

// Register a workflow definition under the given name
func (src *WorkflowDefSrc) Register(name string, wfDef *wfv1.Workflow) {
	if wfDef == nil {
		panic("wf definition is nil")
	}
	src.defs[name] = *wfDef
}

// GetWorkflow looks up workflow definition, and return the definition if
// found. nil is returned if not found.
func (src WorkflowDefSrc) GetWorkflow(wfName string) *wfv1.Workflow {
	wf, ok := src.defs[wfName]
	if !ok {
		return nil
	}
	return wf.DeepCopy()
}
