package mock

import (
	"encoding/json"
	"time"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/adapters"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/utils"
)

// K8SCredSecretStore is a mock up of ports.K8SCredSecretStore
type K8SCredSecretStore struct {
	Key []byte
	K8SSecretClient
}

// Init implements ports.K8SCredSecretStore, the config parameter is ignored
func (store *K8SCredSecretStore) Init(conf config.K8SConfig) error {
	store.Key = utils.GenerateAES256Key()
	store.K8SSecretClient = NewK8SSecretClient()
	store.K8SSecretClient.Init(conf)
	return nil
}

// SetKey set the encryption key
func (store *K8SCredSecretStore) SetKey(key []byte) {
	store.Key = key
}

// StoreEncryptedCred implements ports.K8SCredSecretStore
func (store *K8SCredSecretStore) StoreEncryptedCred(metadata ports.CloudCredMetadata, credBytes []byte, validator ports.CredValidator) (string, error) {
	credStr, err := utils.AESDecrypt(store.Key, credBytes)
	if err != nil {
		return "", err
	}

	var cred map[string]string
	err = json.Unmarshal(credStr, &cred)
	if err != nil {
		return "", err
	}

	if validator != nil {
		//validator()
	}

	return store.StoreCred(metadata, cred)
}

// StoreCred implements ports.K8SCredSecretStore
func (store K8SCredSecretStore) StoreCred(metadata ports.CloudCredMetadata, cred map[string]string) (string, error) {

	labels := map[string]string{
		"cloudType": "openstack",
		"user":      metadata.Username,
		"credID":    metadata.CredID,
		"provider":  metadata.Provider.String(),
	}
	annotations := map[string]string{
		"dateUpdated": time.Now().Format(time.RFC3339),
	}

	secretName := adapters.CredK8SSecretName(metadata)
	return secretName, adapters.CreateOrUpdateCredSecret(&store.K8SSecretClient, secretName, cred, labels, annotations)
}

// Get implements ports.K8SCredSecretStore
func (store K8SCredSecretStore) Get(metadata ports.CloudCredMetadata, result interface{}) error {
	secret, err := store.K8SSecretClient.Get(adapters.CredK8SSecretName(metadata))
	if err != nil {
		return err
	}

	return mapstructure.Decode(secret.Data, result)
}

// Delete implements ports.K8SCredSecretStore
func (store K8SCredSecretStore) Delete(metadata ports.CloudCredMetadata) error {
	return store.K8SSecretClient.Delete(adapters.CredK8SSecretName(metadata))
}
