package mock

import (
	"container/list"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// EventSink is in-mem mock-up of ports.EventSink
type EventSink struct {
	events *list.List
}

// NewMockEventSink creates a MockEventSink
func NewMockEventSink() EventSink {
	sink := EventSink{}
	sink.events = list.New()
	return sink
}

// InitSink implements ports.EventSink
func (sink EventSink) InitSink(conf config.NatsConfig) {
}

// GetEvent returns an event that the sink has received and remove it from
// sink, start from earliest events
func (sink EventSink) GetEvent() types.Event {
	if sink.events.Len() == 0 {
		return nil
	}
	front := sink.events.Front()
	event := front.Value.(types.Event)
	sink.events.Remove(front)

	return event
}

// Publish publish an event to this sink
func (sink EventSink) Publish(event types.Event) error {
	sink.events.PushBack(event)
	return nil
}

// EventSrc is a mock source of cloudevents, implements ports.EventSrc
type EventSrc struct {
	events   *list.List
	Handlers map[string]ports.MsgHandler
}

// NewMockEventSrc creates a new MockEventSrc
func NewMockEventSrc() EventSrc {
	src := EventSrc{}
	src.events = list.New()
	src.Handlers = make(map[string]ports.MsgHandler)
	return src
}

// Publish event
func (src EventSrc) Publish(event cloudevents.Event) error {
	src.events.PushBack(event)
	return nil
}

// RegisterHandler register handler to a specific event type
func (src EventSrc) RegisterHandler(eventType string, handler ports.MsgHandler) error {
	src.Handlers[eventType] = handler
	return nil
}

// Subscribe with Registered handlers
func (src EventSrc) Subscribe() error {
	for src.events.Len() > 0 {

		front := src.events.Front()
		event := front.Value.(cloudevents.Event)
		src.events.Remove(front)

		handler, ok := src.Handlers[event.Type()]
		if !ok {
			return nil
		}

		err := handler(&event)
		if err != nil {
			log.Error(err)
		}
	}
	return nil
}

// SubscribeWithHandler subscribe to all event types with the giving handler
func (src EventSrc) SubscribeWithHandler(handler ports.MsgHandler) error {
	for src.events.Len() > 0 {

		front := src.events.Front()
		event := front.Value.(cloudevents.Event)
		src.events.Remove(front)

		err := handler(&event)
		if err != nil {
			log.Error(err)
		}
	}
	return nil
}
