package adapters

import (
	"gitlab.com/cyverse/cacao-common/messaging"
	"testing"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// Test Encode and Decod of WorkflowCreate
func TestWorkflowCreateCmdEncode(t *testing.T) {
	provider := types.Provider("provider-" + xid.New().String())
	username := "test_user1"
	wfFilename := "test_workflow.yml"
	payload := make(map[string]interface{})
	payload["key"] = "value"

	// Encode
	event := types.WorkflowCreate{
		Provider:         provider,
		Username:         &username,
		WorkflowFilename: wfFilename,
		WfDat:            payload,
	}

	ce, err := messaging.CreateCloudEvent(event, string(event.EventType()), "test-src")
	if err != nil {
		t.Fatal(err)
	}

	if ce.DataContentType() != "application/json" {
		t.Fatalf("wrong content type, %s", ce.DataContentType())
	}

	_, err = ce.MarshalJSON()
	if err != nil {
		t.Fatal(err)
	}

	// Decode
	decodedEvent := EventUnmarshal(log.StandardLogger(), &ce)
	if decodedEvent == nil {
		t.Fatal("Should return decoded event")
	}
	if decodedEvent.EventType() != types.WorkflowCreateRequestedEvent {
		t.Fatal("Inconsistent event type")
	}
	wfCreate, ok := decodedEvent.(types.WorkflowCreate)
	if !ok {
		t.Fatal("Cannot convert to types.WorkflowCreate")
	}

	if wfCreate.Provider != provider {
		t.Fatalf("Inconsistent provider, %s, %s", wfCreate.Provider, ce)
	}
	if *wfCreate.Username != username {
		t.Fatalf("Inconsistent username, %s", *wfCreate.Username)
	}
	if wfCreate.WorkflowFilename != wfFilename {
		t.Fatalf("Inconsistent workflow filename, %s", wfCreate.WorkflowFilename)
	}
	if wfCreate.WfDat["key"] != "value" {
		t.Fatalf("Inconsistent WfDat, %s", wfCreate.WfDat["key"])
	}
}

// Test Encode and Decode of WorkflowTerminate
func TestWorkflowTerminateCmdEncode(t *testing.T) {
	provider := types.Provider("provider-" + xid.New().String())
	wfName := "test-workflow123"

	// Encode
	event := types.WorkflowTerminate{
		Provider:     provider,
		WorkflowName: wfName,
	}

	ce, err := messaging.CreateCloudEvent(event, string(event.EventType()), "test-src")
	if err != nil {
		t.Fatal(err)
	}

	if ce.DataContentType() != "application/json" {
		t.Fatalf("wrong content type, %s", ce.DataContentType())
	}

	_, err = ce.MarshalJSON()
	if err != nil {
		t.Fatal(err)
	}

	// Decode
	decodedEvent := EventUnmarshal(log.StandardLogger(), &ce)
	if decodedEvent == nil {
		t.Fatal("Should return decoded event")
	}
	if decodedEvent.EventType() != types.WorkflowTerminateRequestedEvent {
		t.Fatal("Inconsistent event type")
	}
	wfTerminate, ok := decodedEvent.(types.WorkflowTerminate)
	if !ok {
		t.Fatal("Cannot convert to types.WorkflowTerminate")
	}

	if wfTerminate.Provider != provider {
		t.Fatalf("Inconsistent provider, %s, %s", wfTerminate.Provider, event)
	}
	if wfTerminate.WorkflowName != wfName {
		t.Fatalf("Inconsistent workflow name, %s", wfTerminate.WorkflowName)
	}

}
