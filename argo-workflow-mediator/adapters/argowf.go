package adapters

import (
	"context"

	"github.com/argoproj/argo-workflows/v3/pkg/apiclient"
	"github.com/argoproj/argo-workflows/v3/pkg/apiclient/workflow"
	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
)

type argoClient struct {
	ctx *ports.ArgoContext
}

// NewArgoClient creates an argoClient
func NewArgoClient() ports.ArgoClient {
	return &argoClient{}
}

func (argoCli *argoClient) Init(conf config.ArgoConfig) {
	argoCli.ctx = ports.CreateArgoContextFromConfig(&conf)
}

func (argoCli argoClient) Context() *ports.ArgoContext {
	return argoCli.ctx
}

// CreateWorkflow creates a workflow with given context and workflow definition
func (argoCli argoClient) CreateWorkflow(wfDef *wfv1.Workflow) (*wfv1.Workflow, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "argoClient.CreateWorkflow",
	})
	ctx, cli := getArgoAPIClient(*argoCli.ctx)

	var req workflow.WorkflowCreateRequest
	req.Namespace = argoCli.ctx.Namespace
	req.InstanceID = ""
	req.ServerDryRun = false
	req.Workflow = wfDef

	wf, err := cli.NewWorkflowServiceClient().CreateWorkflow(ctx, &req)
	if err != nil {
		logger.WithError(err).Error("fail to create argo workflow")
		return nil, err
	}
	logger.WithField("wfName", wf.Name).Info("argo workflow created")
	return wf, nil
}

// TerminateWorkflow terminates a workflow
func (argoCli argoClient) TerminateWorkflow(wfName string) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "argoClient.TerminateWorkflow",
		"wfName":   wfName,
	})
	ctx, cli := getArgoAPIClient(*argoCli.ctx)

	var req workflow.WorkflowTerminateRequest
	req.Namespace = argoCli.ctx.Namespace
	req.Name = wfName

	_, err := cli.NewWorkflowServiceClient().TerminateWorkflow(ctx, &req)
	if err != nil {
		logger.WithError(err).Error("fail to terminate workflow")
		return err
	}
	logger.Info("workflow terminated")
	return nil
}

// DeleteWorkflow deletes a workflow
func (argoCli argoClient) DeleteWorkflow(wfName string) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "argoClient.DeleteWorkflow",
		"wfName":   wfName,
	})
	ctx, cli := getArgoAPIClient(*argoCli.ctx)

	var req workflow.WorkflowDeleteRequest
	req.Namespace = argoCli.ctx.Namespace
	req.Name = wfName

	_, err := cli.NewWorkflowServiceClient().DeleteWorkflow(ctx, &req)
	if err != nil {
		logger.WithError(err).Error("fail to delete workflow")
		return err
	}
	logger.Info("workflow deleted")
	return nil
}

// ResubmitWorkflow resubmit a workflow
func (argoCli argoClient) ResubmitWorkflow(wfName string) (*wfv1.Workflow, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "argoClient.ResubmitWorkflow",
		"oldWfName": wfName,
	})
	ctx, cli := getArgoAPIClient(*argoCli.ctx)

	var req workflow.WorkflowResubmitRequest
	req.Namespace = argoCli.ctx.Namespace
	req.Name = wfName

	wf, err := cli.NewWorkflowServiceClient().ResubmitWorkflow(ctx, &req)
	if err != nil {
		logger.WithError(err).Error("fail to resubmit workflow")
		return wf, err
	}
	logger.WithField("newWfName", wf.Name).Info("workflow resubmitted")
	return wf, nil
}

// WorkflowStatus fetches status of a workflow
func (argoCli argoClient) WorkflowStatus(wfName string) (*wfv1.WorkflowStatus, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "argoClient.WorkflowStatus",
		"wfName":   wfName,
	})
	ctx, cli := getArgoAPIClient(*argoCli.ctx)

	var req workflow.WorkflowGetRequest
	req.Namespace = argoCli.ctx.Namespace
	req.Name = wfName
	req.Fields = "status.phase"

	wf, err := cli.NewWorkflowServiceClient().GetWorkflow(ctx, &req)
	if err != nil {
		logger.WithError(err).Error("fail to get workflow")
		return nil, err
	}
	logger.WithField("status", wf.Status).Trace("get workflow")
	return &wf.Status, nil
}

// WorkflowPodLogs fetches logs for a pod in the workflow
func (argoCli argoClient) WorkflowPodLogs(wfName string, podName string) ([]string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "argoClient.WorkflowPodLogs",
		"wfName":   wfName,
		"podName":  podName,
	})
	ctx, cli := getArgoAPIClient(*argoCli.ctx)

	var req workflow.WorkflowLogRequest
	req.Namespace = argoCli.ctx.Namespace
	req.Name = wfName
	req.PodName = podName
	req.LogOptions.Timestamps = true

	logCli, err := cli.NewWorkflowServiceClient().PodLogs(ctx, &req)
	if err != nil {
		logger.WithError(err).Error("fail to get pod logs")
		return nil, err
	}
	entries := make([]string, 1)
	for err == nil {
		var entry *workflow.LogEntry
		entry, err = logCli.Recv()
		if entry != nil {
			entries = append(entries, entry.Content)
		}
	}
	if err != nil {
		logger.WithError(err).Error("err while receiving log entries")
		return nil, err
	}
	return entries, nil
}

// getClient returns an API client to the gRPC API, and the context to use it with
func getArgoAPIClient(argoCtx ports.ArgoContext) (context.Context, apiclient.Client) {

	opts := apiclient.Opts{
		ArgoServerOpts: apiclient.ArgoServerOpts{
			URL:                argoCtx.ServerURL,
			Path:               "",
			Secure:             argoCtx.Secure,
			InsecureSkipVerify: !argoCtx.SSLVerify,
			HTTP1:              false,
		},
		InstanceID:           "",
		AuthSupplier:         func() string { return "Bearer " + argoCtx.Token },
		ClientConfigSupplier: nil,
		Offline:              false,
	}
	ctx, cli, err := apiclient.NewClientFromOpts(opts)
	if err != nil {
		log.Fatal(err)
	}
	return ctx, cli
}
