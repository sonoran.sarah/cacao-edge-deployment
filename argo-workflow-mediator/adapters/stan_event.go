package adapters

import (
	"encoding/json"
	"gitlab.com/cyverse/cacao-common/messaging"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/stan.go"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// EventUnmarshaler unmarshals cloudevents into domain events
type EventUnmarshaler func(*log.Logger, *cloudevents.Event) types.Event

// StanEventAdapter is an adapter for NATS Streaming, and implements ports.EvenPort
type StanEventAdapter struct {
	src  *stanSrc
	sink *stanSink
	// unmarshal cloudevent into domain data types
	eventUnmarshal EventUnmarshaler
	// reuse connection between src and sink
	reuseConn bool
	// STAN client ID and cloudevent source
	clientID string
}

// NewStanEventAdapter creates a new StanEventAdapter.
// if reuseConn is true, connection will be reused between incoming and
// outgoing (connection will be shared irregardless of whether they are
// initialized with the same STAN connection config).
// eventUnmarshal is a function that unmarshals cloudevents into domain
// events.
// clientID is the NATS Streaming client ID, also the cloudevent source field.
// if clientID is empty, a generated one will be used.
func NewStanEventAdapter(reuseConn bool, eventUnmarshal EventUnmarshaler, clientID string) *StanEventAdapter {
	adapter := StanEventAdapter{}
	adapter.reuseConn = reuseConn
	adapter.eventUnmarshal = eventUnmarshal
	if clientID != "" {
		adapter.clientID = clientID
	} else {
		adapter.clientID = "awm-" + xid.New().String()
	}
	return &adapter
}

// InitSrc implements ports.EventSrc and initialize the adapter for incoming events.
func (adapter *StanEventAdapter) InitSrc(conf config.NatsConfig) {
	adapter.src = new(stanSrc)
	adapter.src.receiveSubject = conf.Subject
	adapter.src.queue = conf.Queue

	adapter.src.eventUnmarshal = adapter.eventUnmarshal

	if adapter.reuseConn && adapter.sink != nil && adapter.sink.sc != nil {
		adapter.src.sc = adapter.sink.sc
	} else {
		sc, err := adapter.connect(conf)
		if err != nil {
			log.Fatal(err)
		}
		adapter.src.sc = sc
	}
}

// InitSink implements ports.EventSink and initialize the adapter for outgoing events.
func (adapter *StanEventAdapter) InitSink(conf config.NatsConfig) {
	adapter.sink = new(stanSink)
	adapter.sink.sendSubject = conf.Subject
	adapter.sink.queue = conf.Queue
	adapter.sink.clientID = adapter.clientID

	if adapter.reuseConn && adapter.src != nil && adapter.src.sc != nil {
		adapter.sink.sc = adapter.src.sc
	} else {
		sc, err := adapter.connect(conf)
		if err != nil {
			log.Fatal(err)
		}
		adapter.sink.sc = sc
	}
}

func (adapter StanEventAdapter) connect(conf config.NatsConfig) (stan.Conn, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapter",
		"function":  "StanEventAdapter.connect",
		"natsURL":   conf.NatsURL,
		"clusterID": conf.ClusterID,
		"clientID":  adapter.clientID,
	})
	sc, err := stan.Connect(
		conf.ClusterID,
		adapter.clientID,
		stan.NatsURL(conf.NatsURL),
		stan.ConnectWait(5*time.Second),
	)
	if err != nil {
		logger.WithError(err).Error("fail to connect to STAN")
		return nil, err
	}
	logger.Info("STAN connection established")

	return sc, nil
}

// InitChannel initialize the channel for incoming events.
func (adapter *StanEventAdapter) InitChannel(c chan<- types.Event) {
	if adapter.src == nil {
		log.Fatal("Need to call InitSrc() on StanEventAdapter first")
	}
	adapter.src.eventChan = c
}

// Start implements ports.EventSrc
func (adapter *StanEventAdapter) Start() error {
	return adapter.src.Start()
}

// Publish implements ports.EventSink
func (adapter StanEventAdapter) Publish(event types.Event) error {
	return adapter.sink.Publish(event)
}

// Close closes the NATS Streaming connection
func (adapter StanEventAdapter) Close() error {

	err := adapter.sink.sc.Close()
	if err != nil {
		return err
	}
	return adapter.src.sc.Close()
}

// stanSrc is for incoming events
type stanSrc struct {
	receiveSubject string
	queue          string
	sub            stan.Subscription
	sc             stan.Conn
	eventUnmarshal EventUnmarshaler
	eventChan      chan<- types.Event
}

func (src *stanSrc) Start() error {
	logger := log.WithFields(log.Fields{
		"package":  "adapter",
		"function": "stanSrc.Start",
		"subject":  src.receiveSubject,
		"queue":    src.queue,
	})

	if src.receiveSubject == "" {
		logger.Fatal("cannot subscribe with no receive subject")
	}
	if src.eventUnmarshal == nil {
		logger.Fatal("eventUnmarshal is nil")
	}
	if src.eventChan == nil {
		logger.Fatal("eventChan is nil")
	}

	callback := func(msg *stan.Msg) {
		msg.Ack()

		ce, err := messaging.ConvertStan(msg)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to get event from CloudEvent")
			return
		}
		tid := messaging.GetTransactionID(&ce)
		newLogger := logger.WithFields(log.Fields{
			"eventType": ce.Type(),
			"source":    ce.Source(),
			"tid":       tid,
		})
		if tid == "" {
			newLogger.WithField("eventType", ce.Type()).Warn("event has no transaction ID")
		}
		newLogger.Trace("received message")

		event := src.eventUnmarshal(logger.Logger, &ce)
		if event != nil {
			newLogger.Trace("event unmarshalled")
			src.eventChan <- event
		}
	}

	subOptions := getStanOptions(src.receiveSubject)
	sub, err := src.sc.QueueSubscribe(src.receiveSubject, src.queue, callback, subOptions...)
	if err != nil {
		return err
	}
	src.sub = sub
	log.Info("Start NATS Streaming subscriber")
	return nil
}

func getStanOptions(subject string) []stan.SubscriptionOption {
	aw, _ := time.ParseDuration("60s")
	return []stan.SubscriptionOption{
		stan.DurableName("Durable" + subject),
		stan.MaxInflight(25),
		stan.SetManualAckMode(),
		stan.AckWait(aw),
	}
}

// stanSink is for outgoing events
type stanSink struct {
	sendSubject string
	queue       string
	clientID    string
	sc          stan.Conn
}

func (sink stanSink) Publish(event types.Event) error {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "stanSink.Publish",
		"eventType": event.EventType(),
		"tid":       event.Transaction(),
	})
	ceSource := types.CloudEventSourcePrefix + sink.clientID
	ce, err := messaging.CreateCloudEventWithTransactionID(event, string(event.EventType()), ceSource, event.Transaction())
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}

	msg, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Error("fail to marshal to JSON")
		return err
	}

	err = sink.sc.Publish(sink.sendSubject, msg)
	if err != nil {
		logger.WithError(err).Error("fail to publish")
		return err
	}
	logger.Info("event published")
	return nil
}

// EventUnmarshal is of type EventUnmarshaler, which unmarshals cloudevents
// into apprioate domain events. Returns nil if there is no corrsponding
// domain event for the cloudevent.
func EventUnmarshal(logger *log.Logger, ce *cloudevents.Event) types.Event {
	eventType := types.EventType(ce.Type())

	switch eventType {
	case types.WorkflowCreateRequestedEvent:
		var event types.WorkflowCreate
		err := json.Unmarshal(ce.Data(), &event)
		if err != nil {
			logger.Error(err)
			return nil
		}
		event.EventID = types.EventID(ce.ID())
		event.TransactionID = messaging.GetTransactionID(ce)
		return event

	case types.WorkflowTerminateRequestedEvent:
		var event types.WorkflowTerminate
		err := json.Unmarshal(ce.Data(), &event)
		if err != nil {
			logger.Error(err)
			return nil
		}
		event.EventID = types.EventID(ce.ID())
		event.TransactionID = messaging.GetTransactionID(ce)
		return event

	case types.WorkflowResubmitRequestedEvent:
		var event types.WorkflowResubmit
		err := json.Unmarshal(ce.Data(), &event)
		if err != nil {
			logger.Error(err)
			return nil
		}
		event.EventID = types.EventID(ce.ID())
		event.TransactionID = messaging.GetTransactionID(ce)
		return event

	default:
		logger.WithField("event_type", eventType).Trace("unsupported event type")
	}
	return nil
}
