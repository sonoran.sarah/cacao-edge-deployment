package adapters

import (
	"io/ioutil"
	"path"

	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"github.com/argoproj/argo-workflows/v3/workflow/common"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// WorkflowDefFile load from Workflow Def File from a directory
type WorkflowDefFile struct {
	provider types.Provider
	baseDir  string
}

// NewWorkflowDefFile creates a WorkflowDefFile
func NewWorkflowDefFile(provider types.Provider) ports.WorkflowDefSrc {
	return &WorkflowDefFile{provider: provider}
}

// Init implements ports.WorkflowDefSrc, and initialize this adapter
func (src *WorkflowDefFile) Init(conf config.WorkflowConfig) error {
	src.baseDir = conf.WorkflowBaseDir
	return nil
}

// GetWorkflow lookup workflow definition files by filename, and returns the
// workflow definition if found. nil is returned if not found or error.
func (src WorkflowDefFile) GetWorkflow(wfName string) *wfv1.Workflow {

	wfPath := path.Join(src.baseDir, src.provider.String(), wfName)

	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "WorkflowDefFile.GetWorkflow",
		"path":     wfPath,
	})

	dat, err := ioutil.ReadFile(wfPath)
	if err != nil {
		logger.WithField("error", err).Error("fail to load workflow definition from file")
		return nil
	}

	wfDefs, err := common.SplitWorkflowYAMLFile(dat, true)
	if err != nil {
		logger.WithField("error", err).Error("fail to parse workflow definition as YAML")
		return nil
	}
	if len(wfDefs) > 1 {
		logger.Fatalf("%s contains more than 1 workflow", wfPath)
		return nil
	} else if len(wfDefs) == 0 {
		logger.Fatalf("%s contains no workflow", wfPath)
		return nil
	}
	return &wfDefs[0]
}
