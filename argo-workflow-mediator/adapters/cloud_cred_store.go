package adapters

import (
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/utils"
	coreV1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// K8SCredSecretStore is a storage for cloud credential, implements ports.K8SCredSecretStore
type K8SCredSecretStore struct {
	key                 []byte
	k8sSecret           ports.K8SSecretClient
	hexEncodeSecretName bool
}

// NewK8SCredSecretStore creates a new K8SCredSecretStore
func NewK8SCredSecretStore(envConf config.EnvConfig) ports.K8SCredSecretStore {

	key, err := base64.StdEncoding.DecodeString(envConf.CredEncryptKeyBase64)
	if err != nil {
		log.Fatal(err)
		return nil
	}
	return &K8SCredSecretStore{key: key, hexEncodeSecretName: envConf.HexEncodeSecretName}
}

// Init from config
func (store *K8SCredSecretStore) Init(k8sConf config.K8SConfig) error {
	store.k8sSecret = &K8SSecretClient{}
	return store.k8sSecret.Init(k8sConf)
}

// StoreEncryptedCred stores encrypted credential as K8S secret.
// Returns the name of the K8S secret created on success.
func (store K8SCredSecretStore) StoreEncryptedCred(metadata ports.CloudCredMetadata, credBytes []byte, validator ports.CredValidator) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "K8SCredSecretStore.StoreEncryptedCred",
		"provider": metadata.Provider,
		"username": metadata.Username,
		"credID":   metadata.CredID,
	})
	credStr, err := utils.AESDecrypt(store.key, credBytes)
	if err != nil {
		logger.WithError(err).Error("fail to decrypt")
		return "", err
	}

	var cred map[string]string
	err = json.Unmarshal(credStr, &cred)
	if err != nil {
		logger.WithError(err).Error("JSON unmarshal failed")
		return "", err
	}

	if validator != nil {
		err = validator(cred)
		if err != nil {
			logger.WithError(err).Error("validator returns err")
			return "", err
		}
	}

	secretName, err := store.StoreCred(metadata, cred)
	if err != nil {
		logger.WithError(err).Error("fail to store cred as k8s secret")
		return "", err
	}
	logger.Info("cred k8s secret created/updated")
	return secretName, nil
}

// StoreCred stores credential as K8S secret.
// Returns the name of the K8S secret created on success.
func (store K8SCredSecretStore) StoreCred(metadata ports.CloudCredMetadata, cred map[string]string) (string, error) {

	labels := map[string]string{
		"user":     stripStringForLabel(metadata.Username),
		"credID":   stripStringForLabel(metadata.CredID),
		"provider": metadata.Provider.String(),
	}
	annotations := map[string]string{
		"dateUpdated": time.Now().Format(time.RFC3339),
	}

	secretName := CredK8SSecretName(metadata, store.hexEncodeSecretName)
	return secretName, CreateOrUpdateCredSecret(store.k8sSecret, secretName, cred, labels, annotations)
}

// Get retrieve credential from K8S secrets
func (store K8SCredSecretStore) Get(metadata ports.CloudCredMetadata, result interface{}) error {
	secret, err := store.k8sSecret.Get(CredK8SSecretName(metadata, store.hexEncodeSecretName))
	if err != nil {
		return err
	}
	err = mapstructure.Decode(secret.Data, result)
	if err != nil {
		return err
	}
	return nil
}

// Delete deletes the K8S secret for the cloud credential
func (store K8SCredSecretStore) Delete(metadata ports.CloudCredMetadata) error {
	return store.k8sSecret.Delete(CredK8SSecretName(metadata, store.hexEncodeSecretName))
}

// CreateOrUpdateCredSecret creates a K8S secret for an OpenStack Credential. If secret already exist, then update.
func CreateOrUpdateCredSecret(cli ports.K8SSecretClient, secretName string, cred map[string]string, labels, annotations map[string]string) error {
	if labels == nil {
		labels = map[string]string{"cloudType": "openstack"}
	} else {
		labels["cloudType"] = "openstack"
	}

	secret := coreV1.Secret{
		Type: coreV1.SecretTypeOpaque,
		ObjectMeta: metaV1.ObjectMeta{
			Name:        secretName,
			Labels:      labels,
			Annotations: annotations,
		},
		StringData: cred,
	}

	// Update secret if already exists
	oldSecret, err := cli.Get(secretName)
	if err == nil && oldSecret != nil {
		err = cli.Update(secret)
		if err != nil {
			return err
		}
	} else {
		err = cli.Create(secret)
		if err != nil {
			return err
		}
	}

	return nil
}

// CredK8SSecretName returns name of the k8s secret for cloud credential, username and credential ID can be optionally encoded in hex.
func CredK8SSecretName(metadata ports.CloudCredMetadata, hexEncode ...bool) string {
	if len(hexEncode) == 0 || (len(hexEncode) > 1 && !hexEncode[0]) {
		// default to no encoding
		return fmt.Sprintf("cred-%s-%s", metadata.Username, metadata.CredID)
	}
	// encode username & credID to ensure no invalid char,
	// the resulting name needs to conform the requirement of k8s object name (alphanumeric + '-')
	return fmt.Sprintf("cred-%s-%s",
		strings.ToLower(hex.EncodeToString([]byte(metadata.Username))),
		strings.ToLower(hex.EncodeToString([]byte(metadata.CredID))),
	)
}

// DataError indicates there is an error in the workflow data
type DataError struct {
	Field string
}

func (err DataError) Error() string {
	return fmt.Sprintf("Error in workflow data, field %s", err.Field)
}

func stripStringForLabel(input string) string {
	var result strings.Builder
	for _, c := range input {
		if c >= '0' && c <= '9' {
			result.WriteRune(c)
		} else if c >= 'a' && c <= 'z' {
			result.WriteRune(c)
		} else if c >= 'A' && c <= 'Z' {
			result.WriteRune(c)
		} else if c == '-' || c == '_' || c == '.' {
			result.WriteRune(c)
		} else {
			break
		}
	}
	return result.String()
}
