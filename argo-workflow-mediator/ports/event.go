package ports

import (
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// EventSrc is an adapter interface for incoming events
type EventSrc interface {
	InitSrc(config.NatsConfig)
	InitChannel(chan<- types.Event)
	Start() error
}

// EventSink is an adapter interface for outgoing events
type EventSink interface {
	InitSink(config.NatsConfig)
	Publish(types.Event) error
}

// EventPort is an interface for both incoming and outgoing events
type EventPort interface {
	EventSrc
	EventSink
}
