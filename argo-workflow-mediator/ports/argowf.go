package ports

import (
	"fmt"

	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
)

// ArgoClient is a client interface for manipulating Argo Workflow
type ArgoClient interface {
	Init(conf config.ArgoConfig)
	Context() *ArgoContext
	CreateWorkflow(wfDef *wfv1.Workflow) (*wfv1.Workflow, error)
	TerminateWorkflow(wfName string) error
	DeleteWorkflow(wfName string) error
	ResubmitWorkflow(wfName string) (*wfv1.Workflow, error)
	WorkflowStatus(wfName string) (*wfv1.WorkflowStatus, error)
}

// ArgoContext is the context of which workflows exists in
type ArgoContext struct {
	ServerURL string
	Secure    bool
	SSLVerify bool
	Namespace string
	Token     string
}

// CreateArgoContextFromConfig creates a context from Config
func CreateArgoContextFromConfig(conf *config.ArgoConfig) *ArgoContext {

	if conf == nil {
		return nil
	}

	var url string
	if conf.Port == 443 {
		url = conf.Host
	} else {
		url = fmt.Sprintf("%s:%d", conf.Host, conf.Port)
	}
	return &ArgoContext{
		ServerURL: url,
		Secure:    conf.SSL,
		SSLVerify: conf.SSLVerify,
		Namespace: conf.Namespace,
		Token:     conf.Token,
	}
}
