package ports

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
)

// MsgHandler ...
type MsgHandler func(*cloudevents.Event) error
