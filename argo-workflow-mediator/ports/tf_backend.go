package ports

import "gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"

// TFState is Terraform State of a template
type TFState interface {
	ToStr() string
}

// TFBackendCredential is the credential to connect to a Terraform backend
type TFBackendCredential interface {
}

// TFStateKey is key/identifier to fetch Terraform state from backend
type TFStateKey struct {
	DeploymentID string
}

// String ...
func (k TFStateKey) String() string {
	return k.DeploymentID
}

// TFBackend is Terraform backend
type TFBackend interface {
	Init(config.TFBackendConfig) error
	LookupUser(username string) bool
	SetupUser(username string) error
	GetUserBackendCredential(username string) (TFBackendCredential, error)
	GetHCLBackendBlock(username, workspace string) (string, error)
	GetState(username string, key TFStateKey) (TFState, error)
	PushState(username string, key TFStateKey, state TFState) error
}
