package ports

import (
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	corev1 "k8s.io/api/core/v1"
)

// K8SSecretClient is client for K8S Secret
type K8SSecretClient interface {
	Init(config.K8SConfig) error
	Create(corev1.Secret) error
	Get(name string) (*corev1.Secret, error)
	Update(secret corev1.Secret) error
	Delete(name string) error
}
