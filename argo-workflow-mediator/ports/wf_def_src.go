package ports

import (
	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
)

// WorkflowDefSrc is a source of Workflow Definition
type WorkflowDefSrc interface {
	Init(config.WorkflowConfig) error
	GetWorkflow(wfName string) *wfv1.Workflow
}
