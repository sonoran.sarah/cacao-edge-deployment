package ports

import (
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// K8SCredSecretStore stores cloud credential as K8S secrets
type K8SCredSecretStore interface {
	Init(config.K8SConfig) error
	StoreEncryptedCred(CloudCredMetadata, []byte, CredValidator) (string, error)
	Get(metadata CloudCredMetadata, result interface{}) error
	Delete(CloudCredMetadata) error
}

// CloudCredMetadata is the metadata for cloud credential.
type CloudCredMetadata struct {
	// the cloud provider
	types.Provider
	// owner of the credential
	Username string
	// ID of the credential
	CredID string
}

// CredValidator is a function that validates credential
type CredValidator func(cred map[string]string) error
