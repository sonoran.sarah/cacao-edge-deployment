#!/bin/bash

if [ $# -lt 1 ] || [[ "$1" != "local"  && "$1" != "prod" && "$1" != "ci" ]]; then
    echo "Usage: $0 [local|prod|ci]"
	exit 1
fi

CACAO_DEPLOY_ENVIRONMENT="$1"
if [ "$CACAO_DEPLOY_ENVIRONMENT" == "prod" ] && [ ! -f "hosts.yml" ]; then
    printf "No hosts.yml found for production deployment.  Exiting.\n"
    exit 1
fi

CACAO_CONFIG_FILE="config.$CACAO_DEPLOY_ENVIRONMENT.yml"
printf "Deploying to $CACAO_DEPLOY_ENVIRONMENT...\n"

if [ "$CACAO_DEPLOY_ENVIRONMENT" == "local" ]; then
  if [ ! -f "localhost.yml" ] ; then
      cp "example_hosts.yml" localhost.yml
  fi
  if [ -L hosts.yml ]; then
      rm hosts.yml
  fi
  ln -s localhost.yml hosts.yml
fi

if [ ! -f "$CACAO_CONFIG_FILE" ] ; then
    cp "example_config.yml" $CACAO_CONFIG_FILE
fi

if [ -L config.yml ]; then
    rm config.yml
fi

ln -s $CACAO_CONFIG_FILE config.yml

ansible-playbook -i hosts.yml playbook.yml
