#!/usr/bin/python

import os
import json
import yaml

def read_yaml_file(filename):
    with open(filename) as infile:
        raw = infile.read()
        data = yaml.safe_load(raw)
        return data

def write_yaml_file(filename, data):
    with open(filename, 'w') as outfile:
        outfile.write(yaml.safe_dump(data))

def inject_argo_token(config):
    namespace = os.getenv("K8S_NAMESPACE")
    svc_account = os.getenv("ARGO_SVC_ACCOUNT")
    token = os.getenv("ARGO_TOKEN")

    for prov in config["providers"].keys():
        config["providers"][prov]["namespace"] = namespace
        config["providers"][prov]["service_account"] = svc_account
        config["providers"][prov]["token"] = token
    return config

def inject_pg_credential(config):
    pg_db = os.getenv("POSTGRES_DB")
    pg_user = os.getenv("POSTGRES_USER")
    pg_password = os.getenv("POSTGRES_PASSWORD")

    config["tf_backend"]["user_creden_db"] = pg_db
    config["tf_backend"]["admin_user"] = pg_user
    config["tf_backend"]["admin_password"] = pg_password

    return config

def main():
    filename = os.getenv("CONFIG_FILE")

    config = read_yaml_file(filename)

    config = inject_argo_token(config)
    config = inject_pg_credential(config)

    write_yaml_file(filename, config)

main()
