package utils

import (
	"encoding/base64"
	"fmt"
	"strings"
	"text/template"

	log "github.com/sirupsen/logrus"
)

const kubeconfigTemplate = `
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: {{.CertAuthData}}
    server: {{.ServerURL}}
  name: awm-argo-cluster
contexts:
- context:
    cluster: awm-argo-cluster
    user: {{.SvcAccount}}
    namespace: {{.Namespace}}
  name: awm-argo-cluster
current-context: awm-argo-cluster
kind: Config
preferences: {}
users:
- name: {{.SvcAccount}}
  user:
    token: {{.SvcAccountToken}}
`

// SvcAccountKubeConfParam are parameters that are needed to build a kubeconfig from service account token
type SvcAccountKubeConfParam struct {
	CertAuthData    string
	ServerURL       string
	SvcAccount      string
	SvcAccountToken string
	Namespace       string
}

func validateSvcAccountKubeConfParam(param SvcAccountKubeConfParam) error {
	var err error
	if param.CertAuthData == "" {
		return fmt.Errorf("CertAuthData missing from SvcAccountKubeConfParam")
	}
	_, err = base64.StdEncoding.DecodeString(param.CertAuthData)
	if err != nil {
		return fmt.Errorf("cert_auth_data, %v", err)
	}

	if param.ServerURL == "" {
		return fmt.Errorf("ServerURL missing from SvcAccountKubeConfParam")
	}
	if param.SvcAccount == "" {
		return fmt.Errorf("SvcAccount missing from SvcAccountKubeConfParam")
	}
	if param.SvcAccountToken == "" {
		return fmt.Errorf("SvcAccountToken missing from SvcAccountKubeConfParam")
	}
	return nil
}

// BuildKubeconfigFromSvcAccount builds a kubeconfig from a service account in the target cluster
func BuildKubeconfigFromSvcAccount(param SvcAccountKubeConfParam) ([]byte, error) {

	err := validateSvcAccountKubeConfParam(param)
	if err != nil {
		log.WithField("function", "BuildKubeconfigFromSvcAccount").Error(err)
		return nil, err
	}

	t := template.Must(template.New("kubeconfig").Parse(kubeconfigTemplate))

	var kubeconfigBuilder strings.Builder

	err = t.Execute(&kubeconfigBuilder, param)
	if err != nil {
		log.WithField("function", "BuildKubeconfigFromSvcAccount").Error("executing template:", err)
		return nil, err
	}
	return []byte(kubeconfigBuilder.String()), nil
}
