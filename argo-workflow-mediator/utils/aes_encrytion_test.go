package utils

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestAESEncrypt(t *testing.T) {
	var key []byte
	var data []byte

	key = GenerateAES256Key()
	data = generateRandomBytes(2048)

	ciphertext, err := AESEncrypt(key, data)
	assert.NoError(t, err)
	plaintext, err := AESDecrypt(key, ciphertext)
	assert.NoError(t, err)
	if !reflect.DeepEqual(data, plaintext) {
		t.Errorf("AESDecrypt() = %v, want %v", plaintext, data)
	}
}
