# Argo Workflow Mediator

Argo Workflow Mediator (AWM) will facilicate/mediate the interactions (in the form of workflow call) between center and edge cloud provider. This service is mostly specific to the VM aspect of CACAO rather than the container asepct of CACAO. The [workflows](https://gitlab.com/cyverse/cacao-argo-ansible/-/tree/master/workflow) facilitate through AWM are provider-defined rather than user-defined.

This service is event driven. It subscribe to NATS Streaming messages, and perform workflow actions specified in the msg.


## NATS Streaming Message

The NATS messages that this service accepts and emits are JSON encoded [CloudEvents](https://github.com/cloudevents/spec/blob/v1.0.1/json-format.md).

See `install/example_create.json`, `install/example_resubmit.json`, `install/example_terminate.json` for example messages.

### `WorkflowCreateRequested` event
A workflow will be created based the type of workflow specified in the event.
AWM will fetch the workflow definition based on the workflow type and provider specified in the event.
In response of this event, an event of type `WorkflowCreated` or `WorkflowCreateFailed` will be emitted.

### `WorkflowResubmitRequested` event
Same behavior as `argo resubmit`.
A new workflow will be created based on the same workflow definition of the workflow specified in the event.
In response of this event, an event of type `WorkflowTerminated` or `WorkflowTerminateFailed` will be emitted.

### `WorkflowTerminateRequested` event
Same behaviot as `argo terminate`.
The workflow specified in the event will be terminated. If the workflow is running, the expected status of the workflow will be `Failed`. If the workflow has completed, nothing will happen, the response event will contain the status of the workflow.
> Note: for short workflows, there may be a scenario where the workflow is running at the time of receiving event, but the workflow may finish before AWM can terminate it. Thus its status in the response event may not be `Failed`.
In response of this event, an event of type `WorkflowResubmitted` or `WorkflowResubmitFailed` will be emitted.

## Workflows

Workflows definition files should be supplied as file, in a directory structure like this:
```
workflow-def
|
--- provider_1
|   |
|   --- workflow_definition_1.yml
|   |
|   --- workflow_definition_2.yml
--- provider_2
    |
    --- workflow_definition_1.yml
```
where `provider_1` and `provider_2` is the name/uuid of the provider cluster

### Supported Workflows
- terraform
    apply a Terraform module as child module

## Env

|   |   |
|---|---|
| AWM_CONFIG        | path of the config file for Mediator |
| LOG_LEVEL         | log level |

## Config
see `install/example_config.yml`

## Deploy
see `install/README.md`

## Example with nats-box

```bash
kubectl run -i --rm --tty nats-box --image=synadia/nats-box --restart=Never
```

```bash
stan-pub -s nats -c stan workflow "{}"
```

