package main

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/adapters"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/domain"
)

var envConf config.EnvConfig

func init() {
	// env var config
	err := envconfig.Process("", &envConf)
	if err != nil {
		log.Fatalf("load env config failed, %v", err)
	}

	logLevel, err := log.ParseLevel(envConf.LogLevel)
	if err != nil {
		log.Fatalf("fail to parse log level, %v", err)
	}
	log.SetLevel(logLevel)

	// config file
	err = config.LoadGlobalConfigFromFile(envConf.ConfigPath)
	if err != nil {
		log.Fatalf("fail to load config file, %v", err)
	}
	log.Info(fmt.Sprintf("Config loaded from %s", envConf.ConfigPath))
}

func main() {

	svc := createDomain(*config.GlobalConfig())

	err := svc.Init(*config.GlobalConfig())
	if err != nil {
		log.Fatal(err)
	}

	svc.Start()
}

func createDomain(conf config.Config) domain.Domain {
	svc := domain.NewDomain()
	svc.EventWorkerCount = envConf.EventWorkerCount
	svc.EventChannelBufferCapacity = envConf.EventChannelBufferCapacity
	eventAdapter := adapters.NewStanEventAdapter(true, adapters.EventUnmarshal, envConf.PodName)
	svc.EventSink = eventAdapter
	svc.EventSrc = eventAdapter
	for provider := range conf.ProviderConf {
		svc.ArgoClis[provider] = adapters.NewArgoClient()
		svc.WfSrcs[provider] = adapters.NewWorkflowDefFile(provider)
		svc.CredStores[provider] = adapters.NewK8SCredSecretStore(envConf)
	}
	switch conf.TFBackend.TFBackendType {
	case "":
		log.Info("use Terraform Postgres backend")
		svc.TfBackend = adapters.NewTFPostgresBackend()
	case config.TFPGBackend:
		log.Info("use Terraform Postgres backend")
		svc.TfBackend = adapters.NewTFPostgresBackend()
	case config.TFHTTPBackend:
		log.Info("use Terraform HTTP backend")
		svc.TfBackend = adapters.NewTFHTTPBackend()
	default:
		log.Fatal("bad terraform backend type")
	}
	return svc
}
