module gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator

go 1.16

require (
	github.com/argoproj/argo-workflows/v3 v3.1.8
	github.com/cloudevents/sdk-go/v2 v2.3.1
	github.com/golang-jwt/jwt/v4 v4.0.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.10.2
	github.com/mitchellh/mapstructure v1.4.1
	github.com/nats-io/stan.go v0.9.0
	github.com/rs/xid v1.2.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	gitlab.com/cyverse/cacao-common v0.0.0-20210626003308-95bbf4f3fd50
	gopkg.in/yaml.v2 v2.4.0
	k8s.io/api v0.19.6
	k8s.io/apimachinery v0.19.6
	k8s.io/client-go v0.19.6
)
