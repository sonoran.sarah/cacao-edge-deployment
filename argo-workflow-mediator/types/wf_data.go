package types

import (
	"github.com/mitchellh/mapstructure"
	"gitlab.com/cyverse/cacao-common/common"
)

// TerraformParam is the parameter to required to create a Terraform workflow.
// Corresponds to WfDat in WorkflowCreate
type TerraformParam struct {
	Username   string    `mapstructure:"username"`
	Deployment common.ID `mapstructure:"deployment_id"`
	TemplateID common.ID `mapstructure:"template_id"`
	// This will be used to key Terraform State (Terraform Workspace name) in the Terraform backend.
	// Namely, this key is used to look up state in the backend. Typically this will be some derivation of deployment ID.
	TerraformStateKey  string `mapstructure:"tf_state_key"`
	GitURL             string `mapstructure:"git_url"`
	GitTrackedUpStream struct {
		Branch string `mapstructure:"branch"`
		Tag    string `mapstructure:"tag"`
		Commit string `mapstructure:"commit"`
	} `mapstructure:"git_upstream"`
	// Path is path within git repo to the Terraform module
	Path string `mapstructure:"sub_path"`
	// ID of the cloud credential to use
	CloudCredID string                 `mapstructure:"cloud_cred_id"`
	AnsibleVars map[string]interface{} `mapstructure:"ansible_vars"`
	// Encrypted cloud credential encoded in base64
	CloudCredentialBase64 string `mapstructure:"cloud_cred"`
	GitCredID             string `mapstructure:"git_cred_id"`
	GitCredentialBase64   string `mapstructure:"git_cred"`
}

// EncodeAsMap encode the parameters as a map
func (p TerraformParam) EncodeAsMap() map[string]interface{} {
	result := make(map[string]interface{})
	mapstructure.Decode(p, &result)
	return result
}

// OpenStackCredential is credential for OpenStack. This include both
// username/password and Application Credential.
type OpenStackCredential struct {
	IdentityAPIVersion string `json:"OS_IDENTITY_API_VERSION" mapstructure:"OS_IDENTITY_API_VERSION"`
	RegionName         string `json:"OS_REGION_NAME" mapstructure:"OS_REGION_NAME"`
	Interface          string `json:"OS_INTERFACE" mapstructure:"OS_INTERFACE"`
	AuthURL            string `json:"OS_AUTH_URL" mapstructure:"OS_AUTH_URL"`
	ProjectDomainID    string `json:"OS_PROJECT_DOMAIN_ID" mapstructure:"OS_PROJECT_DOMAIN_ID"`
	ProjectDomainName  string `json:"OS_PROJECT_DOMAIN_NAME" mapstructure:"OS_PROJECT_DOMAIN_NAME"`
	ProjectID          string `json:"OS_PROJECT_ID" mapstructure:"OS_PROJECT_ID"`
	ProjectName        string `json:"OS_PROJECT_NAME" mapstructure:"OS_PROJECT_NAME"`
	UserDomainName     string `json:"OS_USER_DOMAIN_NAME" mapstructure:"OS_USER_DOMAIN_NAME"`

	Username string `json:"OS_USERNAME" mapstructure:"OS_USERNAME"`
	Password string `json:"OS_PASSWORD" mapstructure:"OS_PASSWORD"`

	AuthType      string `json:"OS_AUTH_TYPE" mapstructure:"OS_AUTH_TYPE"`
	AppCredID     string `json:"OS_APPLICATION_CREDENTIAL_ID" mapstructure:"OS_APPLICATION_CREDENTIAL_ID"`
	AppCredName   string `json:"OS_APPLICATION_CREDENTIAL_NAME" mapstructure:"OS_APPLICATION_CREDENTIAL_NAME"`
	AppCredSecret string `json:"OS_APPLICATION_CREDENTIAL_SECRET" mapstructure:"OS_APPLICATION_CREDENTIAL_SECRET"`
}

// GitCredential is credential to access template def in git repo.
type GitCredential struct {
	Username string `json:"GIT_USERNAME" mapstructure:"GIT_USERNAME"`
	Password string `json:"GIT_PASSWORD" mapstructure:"GIT_PASSWORD"`
}
