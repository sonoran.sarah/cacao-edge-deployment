package types

import "gitlab.com/cyverse/cacao-common/common"

// All event structs should implements Event interface, and all should carry
// a EventID field. The EventID field is only used on incoming (event coming
// into the microservice). On the outgoing direction, the EventID should not
// be used, adapters should populate the ID in the underlying transport
// structure (e.g. cloudevents).

// WorkflowCreate is an event that request the creation of a workflow
type WorkflowCreate struct {
	// Deprecated
	// ID of this event, this field is only used for incoming purpose
	EventID `json:"-"`

	TransactionID common.TransactionID `json:"-"`

	// WorkflowCreateCmd is a command that creates a workflow
	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider Provider `json:"provider"`

	// username of the user, null if not user specific
	Username *string `json:"username"`

	// filename of the workflow definition
	WorkflowFilename string `json:"workflow_type"`

	// workflow data, parameters and other metadata used to create the workflow
	WfDat map[string]interface{} `json:"workflow_data"`
}

// EventType implements Event
func (e WorkflowCreate) EventType() EventType {
	return WorkflowCreateRequestedEvent
}

// Transaction ...
func (e WorkflowCreate) Transaction() common.TransactionID {
	return e.TransactionID
}

// WorkflowCreated is an event emitted when a Workflow is created
type WorkflowCreated struct {
	// Deprecated
	// ID of this event, this field is only used for incoming purpose
	EventID `json:"-"`

	// Deprecated
	// ID of the request event (WorkflowCreate) that caused this event
	RequestID EventID `json:"req_id"`

	TransactionID common.TransactionID `json:"-"`

	// The provider of which the workflow is created in
	Provider Provider `json:"provider"`

	// Name of the created workflow
	WorkflowName string `json:"workflow_name"`
}

// EventType implements Event
func (e WorkflowCreated) EventType() EventType {
	return WorkflowCreatedEvent
}

// Transaction ...
func (e WorkflowCreated) Transaction() common.TransactionID {
	return e.TransactionID
}

// WorkflowCreateFailed is an event emitted when failure occurred during
// workflow creation.
// This event is emitted in response to a WorkflowCreate event.
type WorkflowCreateFailed struct {
	// Deprecated
	// ID of this event, this field is only used for incoming purpose
	EventID `json:"-"`

	// Deprecated
	// ID of the request event (WorkflowCreate) that caused this event
	RequestID EventID `json:"req_id"`

	TransactionID common.TransactionID `json:"-"`

	// The provider that the workflow is attempted to be created in
	Provider Provider `json:"provider"`

	// Type of the error occurred that cause the creation to fail
	Error EventErrorType `json:"error"`

	// Extra error message
	Msg string `json:"msg"`
}

// EventType implements Event
func (e WorkflowCreateFailed) EventType() EventType {
	return WorkflowCreateFailedEvent
}

// Transaction ...
func (e WorkflowCreateFailed) Transaction() common.TransactionID {
	return e.TransactionID
}

// WorkflowTerminate is an event that request the terminatation of a workflow
type WorkflowTerminate struct {
	// Deprecated
	// ID of this event, this field is only used for incoming purpose
	EventID `json:"-"`

	TransactionID common.TransactionID `json:"-"`

	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider Provider `json:"provider"`

	// name of the workflow
	WorkflowName string `json:"workflow_name"`
}

// EventType implements Event
func (e WorkflowTerminate) EventType() EventType {
	return WorkflowTerminateRequestedEvent
}

// Transaction ...
func (e WorkflowTerminate) Transaction() common.TransactionID {
	return e.TransactionID
}

// WorkflowTerminated is an event emitted when a workflow is terminated via a WorkflowTerminateCmd event
type WorkflowTerminated struct {
	// Deprecated
	// ID of this event, this field is only used for incoming purpose
	EventID `json:"-"`

	// Deprecated
	// ID of the request event (WorkflowTerminate) that caused this event
	RequestID EventID `json:"req_id"`

	TransactionID common.TransactionID `json:"-"`

	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider Provider `json:"provider"`

	// name of the workflow
	WorkflowName string `json:"workflow_name"`

	// status of the workflow
	WorkflowStatus string `json:"workflow_status"`
}

// EventType implements Event
func (e WorkflowTerminated) EventType() EventType {
	return WorkflowTerminatedEvent
}

// Transaction ...
func (e WorkflowTerminated) Transaction() common.TransactionID {
	return e.TransactionID
}

// WorkflowTerminateFailed is an event emitted when fail to terminate a workflow
type WorkflowTerminateFailed struct {
	// Deprecated
	// ID of this event, this field is only used for incoming purpose
	EventID `json:"-"`

	// Deprecated
	// ID of the request event (WorkflowTerminate) that caused this event
	RequestID EventID `json:"req_id"`

	TransactionID common.TransactionID `json:"-"`

	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider Provider `json:"provider"`

	// name of the workflow
	WorkflowName string `json:"workflow_name"`

	// Type of the error occurred that cause the termination to fail
	Error EventErrorType `json:"error"`

	// Extra error message
	Msg string `json:"msg"`
}

// EventType implements Event
func (e WorkflowTerminateFailed) EventType() EventType {
	return WorkflowTerminateFailedEvent
}

// Transaction ...
func (e WorkflowTerminateFailed) Transaction() common.TransactionID {
	return e.TransactionID
}

// WorkflowResubmit is an event that request the resubmission of a workflow
type WorkflowResubmit struct {
	// Deprecated
	// ID of this event, this field is only used for incoming purpose
	EventID `json:"-"`

	TransactionID common.TransactionID `json:"-"`

	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider Provider `json:"provider"`

	// name of the workflow
	WorkflowName string `json:"workflow_name"`
}

// EventType implements Event
func (e WorkflowResubmit) EventType() EventType {
	return WorkflowResubmitRequestedEvent
}

// Transaction ...
func (e WorkflowResubmit) Transaction() common.TransactionID {
	return e.TransactionID
}

// WorkflowResubmitted is an event that is emitted when workflow resubmission succeeded
type WorkflowResubmitted struct {
	// Deprecated
	// ID of this event, this field is only used for incoming purpose
	EventID `json:"-"`

	// Deprecated
	// ID of the request event (WorkflowResubmit) that caused this event
	RequestID EventID `json:"req_id"`

	TransactionID common.TransactionID `json:"-"`

	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider Provider `json:"provider"`

	// name of the workflow that is requested to be resubmitted
	WorkflowName string `json:"workflow_name"`

	// name of the newly created workflow as the result of the resubmission
	NewWorkflowName string `json:"new_workflow_name"`
}

// EventType implements Event
func (e WorkflowResubmitted) EventType() EventType {
	return WorkflowResubmittedEvent
}

// Transaction ...
func (e WorkflowResubmitted) Transaction() common.TransactionID {
	return e.TransactionID
}

// WorkflowResubmitFailed is an event that is emitted when workflow resubmission failed
type WorkflowResubmitFailed struct {
	// Deprecated
	// ID of this event, this field is only used for incoming purpose
	EventID `json:"-"`

	// Deprecated
	// ID of the request event (WorkflowTerminate) that caused this event
	RequestID EventID `json:"req_id"`

	TransactionID common.TransactionID `json:"-"`

	// cloud provider that the workflow operates on, this determines which Argo Cluster to dispatch the workflow to
	Provider Provider `json:"provider"`

	// name of the workflow that is requested to be resubmitted
	WorkflowName string `json:"workflow_name"`

	// Type of the error occurred that cause the resubmission to fail
	Error EventErrorType `json:"error"`

	// Extra error message
	Msg string `json:"msg"`
}

// EventType implements Event
func (e WorkflowResubmitFailed) EventType() EventType {
	return WorkflowResubmitFailedEvent
}

// Transaction ...
func (e WorkflowResubmitFailed) Transaction() common.TransactionID {
	return e.TransactionID
}
