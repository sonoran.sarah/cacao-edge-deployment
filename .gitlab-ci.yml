image: "golang:1.16-stretch"

stages:
  - format
  - lint
  - test
  - release

variables:
  CACAO_ROOT: /go/src/gitlab.com/cyverse/cacao-edge-deployment

before_script:
  - mkdir -p /go/src/gitlab.com/cyverse
  - cp -r $CI_PROJECT_DIR $CACAO_ROOT

# Note: gofmt will recurse to subpackages
.format:
  stage: format
  needs: []
  variables:
    GOFMT: gofmt -s -l -d
  script:
    - cd $CACAO_ROOT
    - $GOFMT ./$SERVICE | tee output.txt
    - test $(cat output.txt | wc -l) -eq 0
format_awm:
  variables:
    SERVICE: argo-workflow-mediator
  extends: .format
format_event_emitter:
  variables:
    SERVICE: workflow-event-emitter
  extends: .format
format_go_git_cli:
  variables:
    SERVICE: go-git-cli
  extends: .format

# Note: go lint will **not** recurse to subpackages
.lint:
  stage: lint
  needs: []
  before_script:
    - go get golang.org/x/lint/golint
    - go get github.com/gordonklaus/ineffassign
    - go install github.com/gordonklaus/ineffassign@latest
    - go get github.com/client9/misspell/cmd/misspell
    - mkdir -p /go/src/gitlab.com/cyverse
    - cp -r $CI_PROJECT_DIR $CACAO_ROOT
  script:
    - cd $CACAO_ROOT
    - for dir in $(go list ./$SERVICE/...); do golint $dir; done | tee output.txt
    - test $(cat output.txt | wc -l) -eq 0
    - cd ./$SERVICE && go mod vendor && ineffassign ./... && rm -r ./vendor && cd -
    - misspell ./$SERVICE
lint_awm:
  variables:
    SERVICE: argo-workflow-mediator
  extends: .lint
  needs: [format_awm]
lint_event_emitter:
  variables:
    SERVICE: workflow-event-emitter
  extends: .lint
  needs: [format_event_emitter]
lint_go_git_cli:
  variables:
    SERVICE: go-git-cli 
  extends: .lint
  needs: [format_go_git_cli]

.test:
  stage: test
  script:
    - cd $CACAO_ROOT/$SERVICE
    - go test -v -covermode=count ./...
  needs: []
awm_test:
  after_script:
    - cd $CACAO_ROOT/argo-workflow-mediator/cli
    - go build
  variables:
    SERVICE: argo-workflow-mediator
  extends: .test
  needs: [lint_awm]
event_emitter_test:
  variables:
    SERVICE: workflow-event-emitter
  extends: .test
  needs: [lint_event_emitter]

.docker_release:
  image: docker:latest
  stage: release
  services:
    - docker:dind
  tags:
    - dind
  only:
    - master
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -f $DOCKERFILE -t $IMAGE_RELEASE_TAG $BUILD_DIR
    - docker push $IMAGE_RELEASE_TAG
awm_release:
  variables:
    BUILD_DIR: $CACAO_ROOT/argo-workflow-mediator
    IMAGE_RELEASE_TAG: $CI_REGISTRY_IMAGE/argo-workflow-mediator:latest
    DOCKERFILE: $CACAO_ROOT/argo-workflow-mediator/Dockerfile
  extends: .docker_release
  needs: [awm_test]
workflow_event_emitter_release:
  variables:
    BUILD_DIR: $CACAO_ROOT/workflow-event-emitter
    IMAGE_RELEASE_TAG: $CI_REGISTRY_IMAGE/workflow-event-emitter:latest
    DOCKERFILE: $CACAO_ROOT/workflow-event-emitter/Dockerfile
  extends: .docker_release
  needs: [event_emitter_test]
go_git_cli_release:
  variables:
    BUILD_DIR: $CACAO_ROOT/go-git-cli
    IMAGE_RELEASE_TAG: $CI_REGISTRY_IMAGE/go-git-cli:latest
    DOCKERFILE: $CACAO_ROOT/go-git-cli/Dockerfile
  extends: .docker_release
  needs: [lint_go_git_cli]
