package adapters

import (
	"time"

	"github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/types"
)

// NewStanEventSink creates a new StanEventSink
func NewStanEventSink() ports.StanEventSink {
	return &stanEventSink{}
}

// implements StanEventSink
type stanEventSink struct {
	subject string
	conn    stan.Conn
}

// Connect connects to NATS Streaming
func (sink *stanEventSink) Connect(conf types.StanConfig) error {
	err := retry(10, 5*time.Second, func() error {
		sc, err := stan.Connect(
			conf.ClusterID,
			conf.ClientID,
			stan.NatsURL(conf.URL),
			stan.ConnectWait(5*time.Second),
		)
		if err != nil {
			log.Error(err)
			return err
		}
		sink.conn = sc
		return nil
	})
	if err != nil {
		return err
	}
	log.WithFields(map[string]interface{}{
		"url":     conf.URL,
		"cluster": conf.ClusterID,
	}).Info("NATS connection established")

	sink.subject = conf.Subject
	return nil
}

// PublishEvent publish event to NATS Streaming
func (sink *stanEventSink) PublishEvent(event types.Event) error {
	ce, err := event.ToCloudEvent()
	if err != nil {
		return err
	}
	msg, err := ce.MarshalJSON()
	if err != nil {
		return err
	}

	err = retry(10, 5*time.Second, func() error {
		err := sink.conn.Publish(sink.subject, msg)
		if err != nil {
			log.Error(err)
			return err
		}
		return nil
	})
	if err != nil {
		return err
	}
	log.WithFields(map[string]interface{}{
		"subject":    sink.subject,
		"event_type": ce.Type(),
		"msg_len":    len(msg)}).Info("published event")

	return nil
}

// Close closes the NATS connection
func (sink *stanEventSink) Close() error {
	return sink.conn.Close()
}

func retry(attempts int, sleep time.Duration, fn func() error) error {
	if err := fn(); err != nil {
		if attempts--; attempts > 0 {
			time.Sleep(sleep)
			return retry(attempts, 2*sleep, fn)
		}
		return err
	}
	return nil
}
