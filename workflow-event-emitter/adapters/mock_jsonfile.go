package adapters

// MockJSONFile is a mock for ports.JSONSrc
type MockJSONFile struct {
	JSONObj map[string]interface{}
}

// Read ...
func (file MockJSONFile) Read() (map[string]interface{}, error) {
	return file.JSONObj, nil
}
