package adapters

import (
	"encoding/json"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/ports"
	"io/ioutil"
)

// NewJSONFile creates a new ports.JSONSrc from a filename
func NewJSONFile(filename string) ports.JSONSrc {
	return jsonFile{filename}
}

type jsonFile struct {
	filename string
}

// Read reads the content of the file as JSON object
func (file jsonFile) Read() (map[string]interface{}, error) {

	dat, err := ioutil.ReadFile(file.filename)
	if err != nil {
		return nil, err
	}

	return jsonObjFromString(dat)
}

// NewJSONSrcFromString creates a new ports.JSONSrc from a JSON string
func NewJSONSrcFromString(jsonStr string) ports.JSONSrc {
	return jsonString{jsonStr}
}

type jsonString struct {
	str string
}

// Read ...
func (j jsonString) Read() (map[string]interface{}, error) {
	return jsonObjFromString([]byte(j.str))
}

func jsonObjFromString(jsonStr []byte) (map[string]interface{}, error) {
	var jsonObj map[string]interface{}
	err := json.Unmarshal(jsonStr, &jsonObj)
	if err != nil {
		return nil, err
	}
	return jsonObj, nil
}
