package adapters

import (
	"fmt"

	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/types"
)

// MockEventSink is a mock for ports.StanEventSink
type MockEventSink struct {
	connected bool
	Events    []types.Event
}

// Connect ...
func (sink *MockEventSink) Connect(conf types.StanConfig) error {
	sink.connected = true
	sink.Events = make([]types.Event, 0)
	return nil
}

// PublishEvent ...
func (sink *MockEventSink) PublishEvent(event types.Event) error {
	if !sink.connected {
		return fmt.Errorf("not connected")
	}
	sink.Events = append(sink.Events, event)
	return nil
}

// Close ...
func (sink MockEventSink) Close() error {
	sink.connected = false
	return nil
}

// ListEvents returns all events received by this mock event sink
func (sink MockEventSink) ListEvents() []types.Event {
	return sink.Events
}
