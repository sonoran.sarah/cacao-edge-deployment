module gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter

go 1.16

require (
	github.com/argoproj/argo v0.0.0-20200806220847-5759a0e198d3
	github.com/cloudevents/sdk-go/v2 v2.3.1
	github.com/go-openapi/spec v0.20.3 // indirect
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/nats-io/stan.go v0.9.0
	github.com/rs/xid v1.2.1
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.1.3
	github.com/stretchr/testify v1.6.1
	gitlab.com/cyverse/cacao-common v0.0.0-20210716040757-f033e3340b87
	k8s.io/api v0.20.4 // indirect
	k8s.io/kube-openapi v0.0.0-20210305164622-f622666832c1 // indirect
)

replace (
	github.com/argoproj/argo => github.com/argoproj/argo v0.0.0-20200806220847-5759a0e198d3 // v2.9.5
	github.com/argoproj/pkg => github.com/argoproj/pkg v0.1.0

	k8s.io/api => k8s.io/api v0.17.8
	k8s.io/apimachinery => k8s.io/apimachinery v0.17.8
	k8s.io/client-go => k8s.io/client-go v0.17.8
)
