package types

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// Event is a common interface for Events
type Event interface {
	EventType() EventType
	TransactionID() common.TransactionID
	ToCloudEvent() (*cloudevents.Event, error)
}

// EventType is a type to represent event operations
type EventType common.EventType

// EventID is the ID of events, this is used to track transaction
type EventID common.EventID

// Provider is the ID of cloud provider
type Provider service.AWMProvider

// EventErrorType is the error type that appear in the failure/error event emitted
type EventErrorType string

// EnvConfig is config from environment variables.
// Include config for NATS Streaming.
type EnvConfig struct {
	StanConfig

	// must be a JSON string (JSON object)
	WorkflowOutput string `envconfig:"WF_OUTPUT"`
	// must be a JSON string (JSON object)
	Metadata string `envconfig:"METADATA"`
}

// StanConfig is config for NATS Streaming
type StanConfig struct {
	URL     string `envconfig:"NATS_URL" default:"nats://nats:4222"`
	Subject string `envconfig:"NATS_SUBJECT" default:"cyverse.events"`

	ClientID  string `envconfig:"NATS_CLIENT_ID"`
	ClusterID string `envconfig:"NATS_CLUSTER_ID" default:"cacao-cluster"`
}
