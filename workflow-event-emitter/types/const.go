package types

// CloudEventSourcePrefix is prefix for source field in cloudevent
const CloudEventSourcePrefix string = "https://gitlab.com/cyverse/argo-event-aggregator/"

// EventTypePrefix is the prefix for EventType
const EventTypePrefix = "org.cyverse.events."

// WorkflowSucceededEvent is the cloudevent name of WorkflowSucceeded
const WorkflowSucceededEvent EventType = EventTypePrefix + "WorkflowSucceeded"

// WorkflowFailedEvent is the cloudevent name of WorkflowFailed
const WorkflowFailedEvent EventType = EventTypePrefix + "WorkflowFailed"
