# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
- add workflow event emitter; passing NATS Streaming config to workflow; change type of provider; [MR](https://gitlab.com/cyverse/cacao-edge-deployment/-/merge_requests/3)
- uses `cacao-common` instead of `cacao-types`; upgrade to go 1.16; support transaction ID; [MR](https://gitlab.com/cyverse/cacao-edge-deployment/-/merge_requests/5)
- rework workflow-event-emitter cmd line interface, pass through metadata and transaction ID to event
- rework terraform workflow data, pass more metadata about the workflow, pass enough metadata to clone the template, accepts git credential
- encode username in terraform postgres backend
- add go-git-cli
- encode username in lower case in terraform postgres backend

### Removed
