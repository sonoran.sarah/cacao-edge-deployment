# cacoa-edge-deployment

This repo contains component that can be (but not necessarily) deployed to the edge cloud provider (not the central provider where most of the CACAO resides).

The goal is to bring the last bit of certain interactions (e.g. VM deployment) closer to the provider to mitigate the overhead or instability of the network between center and edge by making the calls between center and edge coarse and heavy rather than dense and light.

## argo-workflow-mediator
`argo-workflow-mediator` will facilicate/mediate provider-defined workflow between center and edge.
